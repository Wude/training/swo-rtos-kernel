#include "itm.h"

#include "stm32f4xx.h"

#include "core_cm4.h"

#include <stdio.h>

/*****************************************************************************
* Definitions.
******************************************************************************/

/** @brief Send data to the ITM debug port 0.

	@param [in] aData The data to send.
	@param [in] count The number of characters to send.
*/
void ITM_SendData(const char* aData, size_t len) {
	if (aData != NULL) {
		for (size_t i = 0; i < len; ++i) { ITM_SendChar(aData[i]); }
	}
}

/** @brief Send a string to the ITM debug port 0.

	@param [in] sText The string to send.
*/
void ITM_SendString(Str_t sText) {
	if (sText != NULL) {
		while (*sText != '\0') { ITM_SendChar(*sText++); }
	}
}

/** @brief Send a 32-bit unsigned integer to the ITM debug port 0.

	@param [in] value The 32-bit unsigned integer to send.
*/
void ITM_SendUint32(uint32_t value) {
	size_t        count = 10;
	volatile char aChars[count + 1];

	count = sprintf((char*)aChars, "%" PRIu32, value);
	for (size_t i = 0; i < count; ++i) { ITM_SendChar(aChars[i]); }
}
