#include "util.h"
#include <stdint.h>

static const char g_aBitCountTab[] = "\x00\x01\x02\x02\x03\x03\x03\x03\x04\x04\x04\x04\x04\x04\x04\x04"
									 "\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05\x05"
									 "\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06"
									 "\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06\x06"
									 "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07"
									 "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07"
									 "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07"
									 "\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07\x07"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08"
									 "\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08\x08";

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint8_BitCount(uint8_t value) { return g_aBitCountTab[value]; }

/* @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint16_BitCount(uint16_t value) {
	size_t count = 0;
	if (value >= 1U << 8) {
		value >>= 8;
		count = 8;
	}
	return count + g_aBitCountTab[value];
}

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint32_BitCount(uint32_t value) {
	size_t count = 0;
	if (value >= 1U << 16) {
		value >>= 16;
		count = 16;
	}
	if (value >= 1U << 8) {
		value >>= 8;
		count += 8;
	}
	return count + g_aBitCountTab[value];
}

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint64_BitCount(uint64_t value) {
	size_t count = 0;
	if (value >= (1ULL << 32)) {
		value >>= 32;
		count = 32;
	}
	if (value >= 1U << 16) {
		value >>= 16;
		count += 16;
	}
	if (value >= 1U << 8) {
		value >>= 8;
		count += 8;
	}
	return count + g_aBitCountTab[value];
}
