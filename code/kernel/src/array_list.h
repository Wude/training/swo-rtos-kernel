#ifndef __ARRAY_LIST__
#define __ARRAY_LIST__

#include "util.h"

/* The type used for indexing the data in the array lists. */
typedef int32_t ArrayListSize_t;
/* "ArrayListSize_fm" holds the format used for "printf". */
#define ArrayListSize_fm PRIi32
/* "ArrayListSize_size" must match `sizeof(ArrayListSize_t)` */
#define ArrayListSize_size 4

/*******************************************************************************
* generic array list - type dependent parts
********************************************************************************
* The pragma pack is neccessary to ensure the proper alignment
* for item types larger than "ArrayListSize_size".
*******************************************************************************/

#define DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(ArrayListSize_size) \
	/** @brief An array list of items with type "list_type" and size "list_size". */ \
	typedef struct list_type_name { \
		ArrayListSize_t count; \
		item_type       aData[list_size]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	/** @brief Get the list size. */ \
	static inline ArrayListSize_t list_type_name##_Size() { return list_size; } \
	/** @brief Get the number of items in the list. */ \
	static inline ArrayListSize_t list_type_name##_GetCount(list_type* pArrayList) { return pArrayList->count; } \
	/** @brief Initialize the list with an item count. */ \
	void list_type_name##_Init(list_type* pArrayList, ArrayListSize_t count); \
	/** @brief Append a single empty item to the list. */ \
	item_type* list_type_name##_AppendItemEmpty(list_type* pArrayList); \
	/** @brief Append a single item to the list. */ \
	bool list_type_name##_AppendItem(list_type* pArrayList, item_type item); \
	/** @brief Append an array to the list. */ \
	bool list_type_name##_AppendArray(list_type* pArrayList, ArrayListSize_t count, item_type* aData); \
	/** @brief Append another list to the list. */ \
	bool list_type_name##_AppendList(list_type* pArrayList, list_type* other); \
	/** @brief Remove the slice defined by the start and end indexes. */ \
	void list_type_name##_RemoveSlice(list_type* pArrayList, ArrayListSize_t start, ArrayListSize_t end); \
	/** @brief Find the index for the given item. Returns "-1" when not found. */ \
	ArrayListSize_t list_type_name##_FindIndex(list_type* pArrayList, PredicateFn_t fPredicate, void* aData);

#define DEFINE_ARRAY_LIST_BODY(list_type_name, list_type, item_type, list_size) \
\
	void list_type_name##_Init(list_type* pArrayList, ArrayListSize_t count) { pArrayList->count = count; } \
\
	item_type* list_type_name##_AppendItemEmpty(list_type* pArrayList) { \
		if (pArrayList->count >= list_size) { return NULL; } \
		return &pArrayList->aData[pArrayList->count++]; \
	} \
\
	bool list_type_name##_AppendItem(list_type* pArrayList, item_type item) { \
		if (pArrayList->count >= list_size) { return false; } \
		pArrayList->aData[pArrayList->count] = item; \
		pArrayList->count++; \
		return true; \
	} \
\
	bool list_type_name##_AppendArray(list_type* pArrayList, ArrayListSize_t count, item_type* aData) { \
		return ArrayList_AppendArray((ArrayList_t*)pArrayList, sizeof(item_type), list_size, count, aData); \
	} \
\
	bool list_type_name##_AppendList(list_type* pArrayList, list_type* pOther) { \
		return ArrayList_AppendArray( \
			(ArrayList_t*)pArrayList, sizeof(item_type), list_size, pOther->count, pOther->aData); \
	} \
\
	void list_type_name##_RemoveSlice(list_type* pArrayList, ArrayListSize_t start, ArrayListSize_t end) { \
		ArrayList_RemoveSlice((ArrayList_t*)pArrayList, sizeof(item_type), start, end); \
	} \
\
	ArrayListSize_t list_type_name##_FindIndex(list_type* pArrayList, PredicateFn_t fPredicate, void* aData) { \
		return ArrayList_FindIndex((ArrayList_t*)pArrayList, sizeof(item_type), fPredicate, aData); \
	}

#define DEFINE_ARRAY_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_ARRAY_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_ARRAY_LIST_BODY(list_type_name, list_type, item_type, list_size)

/*******************************************************************************
* generic array list - type independent parts
*******************************************************************************/

DEFINE_ARRAY_LIST_TYPE_ONLY(ArrayList, ArrayList_t, char, EMTPY);

/** @brief Append the list by the given array. */
bool ArrayList_AppendArray(
	ArrayList_t* pArrayList, ArrayListSize_t itemSize, ArrayListSize_t size, ArrayListSize_t count, void* aData);

/** @brief Remove a slice from the list. */
void ArrayList_RemoveSlice(
	ArrayList_t* pArrayList, ArrayListSize_t itemSize, ArrayListSize_t start, ArrayListSize_t end);

/** @brief Find the index of an element by a predicate. */
ArrayListSize_t
ArrayList_FindIndex(ArrayList_t* pArrayList, ArrayListSize_t itemSize, PredicateFn_t fPredicate, void* pClosure);

#endif // __ARRAY_LIST__
