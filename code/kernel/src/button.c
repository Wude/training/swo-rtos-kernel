#include "button.h"
#include "kernel.h"
#include "ports.h"

#include "stm32f4xx_gpio.h"

void Button_Init(Button_t* pPin, GPIO_TypeDef* port, short pin, bool normallyClosed) {
	pPin->pPort          = port;
	pPin->pin            = pin;
	pPin->normallyClosed = normallyClosed;
	pPin->lastValue      = normallyClosed;
}

void Button_Update(Button_t* pPin) {
	bool currentValue = GPIO_GetBool(pPin->pPort, pPin->pin);

	// When "normally closed": activating means "negative edge", deactivating means "positive edge"
	// When "normally open": activating means "positive edge", deactivating means "negative edge"

	uint32_t elapsedTimeMs = Kernel_GetElapsedSystemTimeMs();

	if (pPin->lastValue != currentValue) {
		// A change in value marks the start of a new "phase".
		pPin->currentPhaseDurationMs = 0;
		pPin->lastPhaseDurationMs    = elapsedTimeMs - pPin->lastElapsedTimeMs;
		pPin->lastElapsedTimeMs      = elapsedTimeMs;
		pPin->locked                 = false;
		pPin->lastValue              = currentValue;
		pPin->changing               = true;
	} else {
		pPin->currentPhaseDurationMs = elapsedTimeMs - pPin->lastElapsedTimeMs;
		pPin->changing               = false;
	}
}

/** @brief Is the pin currently "active"? */
bool Button_Active(Button_t* pPin) { return pPin->lastValue ^ pPin->normallyClosed; }

/** @brief Is the pin activating - currently changing from "inactive" to "active"? */
bool Button_Activating(Button_t* pPin) { return pPin->changing && Button_Active(pPin); }

/** @brief Is the pin deactivating - currently changing from "active" to "inactive"? */
bool Button_Deactivating(Button_t* pPin) { return pPin->changing && !Button_Active(pPin); }

/** @brief Is the pin deactivating after having been active for less than a certain duration? */
bool Button_DeactivatingBeforeTime(Button_t* pPin, uint32_t durationMs) {
	// A short button press has ended?
	return Button_Deactivating(pPin) && pPin->lastPhaseDurationMs >= 20 && // We want to prevent a trigger "flickering".
		   pPin->lastPhaseDurationMs < durationMs;
}

/** @brief Does the time of the pin being active exceed a certain duration? */
bool Button_ActiveForSomeTime(Button_t* pPin, uint32_t durationMs, bool locking) {
	if (
		// The button is being pressed for a longer period.
		!pPin->locked && Button_Active(pPin) && pPin->currentPhaseDurationMs >= durationMs) {
		pPin->locked = locking;
		return true;
	}
	return false;
}
