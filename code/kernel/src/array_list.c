#include "array_list.h"

#include <stdio.h> //< Used for simple debugging with `printf`.
#include <string.h>

/** @brief Append the list by the given array. */
bool ArrayList_AppendArray(
	ArrayList_t* pArrayList, ArrayListSize_t itemSize, ArrayListSize_t size, ArrayListSize_t count, void* aData) {
	const ArrayListSize_t minsize = pArrayList->count + count + 1;

	if (minsize > size) { return false; }
	memcpy(((char*)&pArrayList->aData[0]) + (itemSize * pArrayList->count), aData, itemSize * (ArrayListSize_t)count);
	pArrayList->count += count;
	return true;
}

/** @brief Remove a slice from the list. */
void ArrayList_RemoveSlice(
	ArrayList_t* pArrayList, ArrayListSize_t itemSize, ArrayListSize_t start, ArrayListSize_t end) {
	if (end > start) {
		if (start < pArrayList->count) { start = 0; }
		if (end < pArrayList->count - 1) {
			/* Copy list rest to the gap start. */
			const ArrayListSize_t restCount = (pArrayList->count - end) * itemSize;
			for (ArrayListSize_t i = start * itemSize; i < restCount; i += itemSize) {
				pArrayList->aData[i] = pArrayList->aData[i + restCount];
			}
			pArrayList->count -= end - start;
		} else {
			/* There is no list rest. */
			pArrayList->count = start;
		}
	}
}

/** @brief Find the index of an element by a predicate. */
ArrayListSize_t
ArrayList_FindIndex(ArrayList_t* pArrayList, ArrayListSize_t itemSize, PredicateFn_t fPredicate, void* pClosure) {
	ArrayListSize_t pos;

	for (pos = 0; pos < pArrayList->count; pos++) {
		if (fPredicate(pClosure, (&pArrayList->aData[0]) + pos * itemSize)) { return pos; }
	}

	return -1;
}
