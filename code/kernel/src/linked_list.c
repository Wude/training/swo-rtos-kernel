#include "linked_list.h"

#include <stdio.h> //< Used for simple debugging with `printf`.

/*****************************************************************************
* Private definitions.
******************************************************************************/

#define GET_ITEM(p_node, offset) (void*)((intptr_t)p_node - offset)

static void InsertBefore(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode) {
	pNewNode->pPrev = pOldNode->pPrev;
	pNewNode->pNext = pOldNode;
	if (pOldNode->pPrev != NULL) { pOldNode->pPrev->pNext = pNewNode; }
	pOldNode->pPrev = pNewNode;
	if (pList->pFirst == pOldNode) { pList->pFirst = pNewNode; }
	++pList->count;
}

static void InsertAfter(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode) {
	pNewNode->pPrev = pOldNode;
	pNewNode->pNext = pOldNode->pNext;
	if (pOldNode->pNext != NULL) { pOldNode->pNext->pPrev = pNewNode; }
	pOldNode->pNext = pNewNode;
	if (pList->pLast == pOldNode) { pList->pLast = pNewNode; }
	++pList->count;
}

static LinkedListNode_t* Split(LinkedListNode_t* left, size_t count) {
	LinkedListNode_t* right = left;
	for (size_t i = 0; i < count; ++i) { right = right->pNext; }
	if (right->pPrev != NULL) {
		right->pPrev->pNext = NULL;
		right->pPrev        = NULL;
	}
	return right;
}

static LinkedListNode_t*
Merge(LinkedListNode_t* pLeft, LinkedListNode_t* pRight, CompareFn_t fCompare, Cmp_t cmpGreater, size_t offset) {
	LinkedListNode_t* pNode;

	// Determine the first node.
	if (fCompare(GET_ITEM(pLeft, offset), GET_ITEM(pRight, offset)) == cmpGreater) {
		pNode  = pRight;
		pRight = pRight->pNext;
	} else {
		pNode = pLeft;
		pLeft = pLeft->pNext;
	}

	LinkedListNode_t* pCurrentNode = pNode;
	for (;;) {
		// Once a partial list is used up, the merge an be shortened.
		if (pLeft == NULL) {
			pRight->pPrev       = pCurrentNode;
			pCurrentNode->pNext = pRight;
			break;
		} else if (pRight == NULL) {
			pLeft->pPrev        = pCurrentNode;
			pCurrentNode->pNext = pLeft;
			break;
		}

		// Append the lists according to the comparison result.
		if (fCompare(GET_ITEM(pLeft, offset), GET_ITEM(pRight, offset)) == cmpGreater) {
			pRight->pPrev       = pCurrentNode;
			pCurrentNode->pNext = pRight;
			pCurrentNode        = pRight;
			pRight              = pRight->pNext;
		} else {
			pLeft->pPrev        = pCurrentNode;
			pCurrentNode->pNext = pLeft;
			pCurrentNode        = pLeft;
			pLeft               = pLeft->pNext;
		}
	}
	return pNode;
}

static LinkedListNode_t*
Sort(LinkedListNode_t* pLeft, size_t count, CompareFn_t fCompare, Cmp_t cmpGreater, size_t offset) {
	if (count <= 1) { return pLeft; }
	LinkedListNode_t* right;
	size_t            half_count = count / 2;

	// Split the list in equally sized parts.
	right = Split(pLeft, half_count);

	// Sort the partial lists.
	pLeft = Sort(pLeft, half_count, fCompare, cmpGreater, offset);
	right = Sort(right, count - half_count, fCompare, cmpGreater, offset);

	// Merge the sorted partial lists.
	return Merge(pLeft, right, fCompare, cmpGreater, offset);
}

/*****************************************************************************
* Definitions.
******************************************************************************/

void LinkedList_Init(LinkedList_t* pList) {
	pList->count  = 0;
	pList->pFirst = NULL;
	pList->pLast  = NULL;
}

void LinkedList_Prepend(LinkedList_t* pList, LinkedListNode_t* pNewNode) {
	pNewNode->pPrev = NULL;
	pNewNode->pNext = pList->pFirst;
	if (pList->pFirst == NULL) {
		pList->pFirst = pNewNode;
	} else if (pList->pFirst != NULL) {
		pList->pFirst->pPrev = pNewNode;
	}
	pList->pFirst = pNewNode;
	++pList->count;
}

void LinkedList_Append(LinkedList_t* pList, LinkedListNode_t* pNewNode) {
	pNewNode->pPrev = pList->pLast;
	pNewNode->pNext = NULL;
	if (pList->pFirst == NULL) {
		pList->pFirst = pNewNode;
	} else if (pList->pLast != NULL) {
		pList->pLast->pNext = pNewNode;
	}
	pList->pLast = pNewNode;
	++pList->count;
}

void LinkedList_InsertBefore(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode) {
	if (pNewNode != NULL) {
		if (pNewNode->pPrev != NULL || pNewNode->pNext != NULL) { LinkedList_Remove(pList, pNewNode); }
		if (pOldNode == NULL) {
			LinkedList_Prepend(pList, pNewNode);
		} else {
			InsertBefore(pList, pOldNode, pOldNode);
		}
	}
}

void LinkedList_InsertAfter(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode) {
	if (pNewNode != NULL) {
		if (pNewNode->pPrev != NULL || pNewNode->pNext != NULL) { LinkedList_Remove(pList, pNewNode); }
		if (pOldNode == NULL) {
			LinkedList_Append(pList, pNewNode);
		} else {
			InsertAfter(pList, pOldNode, pNewNode);
		}
	}
}

void LinkedList_InsertFromFront(
	LinkedList_t* pList, LinkedListNode_t* pNewNode, CompareFn_t fCompare, bool asc, size_t offset) {
	if (pNewNode != NULL) {
		if (pNewNode->pPrev != NULL || pNewNode->pNext != NULL) { LinkedList_Remove(pList, pNewNode); }

		const Cmp_t       cmpGreater = CMP_GREATER(asc);
		LinkedListNode_t* pNode;
		for (pNode = pList->pFirst;
			 pNode != NULL && fCompare(GET_ITEM(pNewNode, offset), GET_ITEM(pNode, offset)) == cmpGreater;
			 pNode = pNode->pNext) { }
		if (pNode == NULL) {
			LinkedList_Append(pList, pNewNode);
		} else {
			InsertBefore(pList, pNode, pNewNode);
		}
	}
}

void LinkedList_InsertFromBehind(
	LinkedList_t* pList, LinkedListNode_t* pNewNode, CompareFn_t fCompare, bool asc, size_t offset) {
	if (pNewNode != NULL) {
		if (pNewNode->pPrev != NULL || pNewNode->pNext != NULL) { LinkedList_Remove(pList, pNewNode); }

		const Cmp_t       cmpGreater = CMP_GREATER(asc);
		Cmp_t             cmp        = 0;
		LinkedListNode_t* pLastNode;
		LinkedListNode_t* pNode;
		for (pLastNode = NULL, pNode = pList->pLast;
			 pNode != NULL && (cmp = fCompare(GET_ITEM(pNode, offset), GET_ITEM(pNewNode, offset))) == cmpGreater;
			 pLastNode = pNode, pNode = pNode->pPrev) { }
		if (pNode == NULL) { pNode = pLastNode; }
		if (pNode == NULL) {
			LinkedList_Append(pList, pNewNode);
		} else if (cmp == cmpGreater) {
			InsertBefore(pList, pNode, pNewNode);
		} else {
			InsertAfter(pList, pNode, pNewNode);
		}
	}
}

void LinkedList_Remove(LinkedList_t* pList, LinkedListNode_t* pNode) {
	if (pList->pFirst == pNode) {
		pList->pFirst = pNode->pNext;
	} else if (pNode->pPrev != NULL) {
		pNode->pPrev->pNext = pNode->pNext;
	}
	if (pList->pLast == pNode) {
		pList->pLast = pNode->pPrev;
	} else if (pNode->pNext != NULL) {
		pNode->pNext->pPrev = pNode->pPrev;
	}
	pNode->pPrev = NULL;
	pNode->pNext = NULL;
	--pList->count;
}

void LinkedList_Sort(LinkedList_t* pList, CompareFn_t fCompare, bool asc, size_t offset) {
	const Cmp_t cmpGreater = CMP_GREATER(asc);
	pList->pFirst          = Sort(pList->pFirst, pList->count, fCompare, cmpGreater, offset);
	LinkedListNode_t* pLastNode;
	LinkedListNode_t* pNode;
	for (pLastNode = NULL, pNode = pList->pFirst; pNode != NULL; pLastNode = pNode, pNode = pNode->pNext) { }
	pList->pLast = pLastNode;
}
