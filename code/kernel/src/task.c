#include "task.h"

#include <stdint.h>
#include <string.h>

/*****************************************************************************
* Definitions.
******************************************************************************/

void PeriodicTask_Init(PeriodicTask_t* pPeriodicTask, Str_t sName, uint8_t periodExponent, uint32_t durationMs) {
	pPeriodicTask->base.taskType  = TaskType_Periodic;
	pPeriodicTask->base.sName     = sName;
	pPeriodicTask->periodExponent = periodExponent;
	pPeriodicTask->durationMs     = durationMs;
}

void SporadicTask_Init(SporadicTask_t* pSporadicTask, Str_t sName) {
	pSporadicTask->base.taskType = TaskType_Sporadic;
	pSporadicTask->base.sName    = sName;
	pSporadicTask->deadline      = 0;
}

void AperiodicTask_Init(AperiodicTask_t* pAperiodicTask, Str_t sName, uint8_t priority) {
	pAperiodicTask->base.taskType = TaskType_Aperiodic;
	pAperiodicTask->base.sName    = sName;
	pAperiodicTask->priority      = priority;
}

Cmp_t SporadicTask_Compare(SporadicTask_t* pLeft, SporadicTask_t* pRight) {
	if (pLeft->deadline < pRight->deadline) { return -1; }
	if (pLeft->deadline > pRight->deadline) { return 1; }
	return 0;
}

Cmp_t AperiodicTask_Compare(AperiodicTask_t* pLeft, AperiodicTask_t* pRight) {
	if (pLeft->priority < pRight->priority) { return -1; }
	if (pLeft->priority > pRight->priority) { return 1; }
	return 0;
}

DEFINE_ARRAY_LIST_BODY(PeriodicTaskList, PeriodicTaskList_t, PeriodicTask_t, PeriodicTaskList_Size())
DEFINE_ARRAY_LIST_BODY(SporadicTaskList, SporadicTaskList_t, SporadicTask_t, SporadicTaskList_Size())
DEFINE_ARRAY_LIST_BODY(AperiodicTaskList, AperiodicTaskList_t, AperiodicTask_t, AperiodicTaskList_Size())
