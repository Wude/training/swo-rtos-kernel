#ifndef __SCHEDULING_TREE_H__
#define __SCHEDULING_TREE_H__

#include "task.h"
#include "util.h"

#include <stdint.h>

/*****************************************************************************
* Types
******************************************************************************/

typedef struct SchedulingTreeSlot SchedulingTreeSlot_t;
typedef struct SchedulingTreeNode SchedulingTreeNode_t;

/** @brief A scheduling tree slot.

	An empty reference to a periodic task means this slot may be used
	for dynamically scheduling aperiodic or sporadic tasks.
*/
struct SchedulingTreeSlot {
	SchedulingTreeSlot_t* pNext;          /**< Next slot or `NULL`. */
	PeriodicTask_t*       pPeriodicTask;  /**< A periodic task or `NULL`. */
	uint32_t              freeDurationMs; /**< Free execution duration. */
};

/** @brief A scheduling tree node. */
struct SchedulingTreeNode {
	SchedulingTreeNode_t* apChilds[2];      /**< Child nodes. Both `NULL` when on last period exponent. */
	SchedulingTreeSlot_t* pSlot;            /**< The slot chain. */
	uint32_t              branchDurationMs; /**< Execution duration for the branch so far. */
};

DEFINE_ARRAY_LIST_SPEC(SchedulingTreeSlotList, SchedulingTreeSlotList_t, SchedulingTreeSlot_t, 64)

#define SCHEDULING_TREE_NODE_LIST_SIZE 64
DEFINE_ARRAY_LIST_TYPE_ONLY(
	SchedulingTreeNodeList, SchedulingTreeNodeList_t, SchedulingTreeNode_t, SCHEDULING_TREE_NODE_LIST_SIZE)

/** @brief A scheduling tree. */
typedef struct SchedulingTree {
	SchedulingTreeNode_t*    pRoot;             /**< The root node. */
	uint32_t                 periodMs;          /**< The period duration of an execution branch . */
	uint32_t                 maxPeriodExponent; /**< The maximum execution period exponent. */
	uint32_t                 branchCount;       /**< The number of execution branches. */
	SchedulingTreeNodeList_t nodes;             /**< The reserve of nodes that may be used. */
	SchedulingTreeSlotList_t slots;             /**< The reserve of slots that may be used. */
} SchedulingTree_t;

/** @brief An iterator for a scheduling tree. */
typedef struct SchedulingTreeIterator {
	SchedulingTree_t*     pTree;       /**< The scheduling tree to iterate. */
	SchedulingTreeNode_t* pNode;       /**< The currently active node. */
	SchedulingTreeSlot_t* pSlot;       /**< The currently active slot. */
	uint32_t              cycleCount;  /**< The number of times all branches have been iterated. */
	uint32_t              branchIndex; /**< The index of the currently active execution branch. */
	uint32_t              periodIndex; /**< The index of the currently active task period index. */
} SchedulingTreeIterator_t;

/*****************************************************************************
* Definitions
******************************************************************************/

/** @brief Initialize a scheduling tree.

	@param [in] pTree The scheduling tree to initialize.
	@param [in] periodMs The period duration per execution branch.
	@param [in] pPeriodicTasks The periodic tasks to use.
	@return `NULL` or the reason for failing as a string.
*/
ErrorReasonStr_t SchedulingTree_Init(SchedulingTree_t* pTree, uint32_t periodMs, PeriodicTaskList_t* pPeriodicTasks);

SchedulingTreeSlot_t* SchedulingTreeIterator_Init(SchedulingTreeIterator_t* pIterator, SchedulingTree_t* pTree);

SchedulingTreeSlot_t* SchedulingTreeIterator_Next(SchedulingTreeIterator_t* pIterator);

#endif // __SCHEDULING_TREE_H__
