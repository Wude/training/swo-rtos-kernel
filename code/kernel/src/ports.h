#ifndef __PORTS__
#define __PORTS__

#include "util.h"

#include "stm32f4xx.h"

void Ports_Init();
bool GPIO_GetBool(GPIO_TypeDef* pPort, short pin);
void GPIO_SetBool(GPIO_TypeDef* pPort, short pin);
void GPIO_ResetBool(GPIO_TypeDef* pPort, short pin);
void GPIO_ToggleBool(GPIO_TypeDef* pPort, short pin);

#endif // __PORTS__
