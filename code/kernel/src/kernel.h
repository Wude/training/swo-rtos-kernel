#ifndef __KERNEL_H__
#define __KERNEL_H__

#include "array_list.h"
#include "buffer.h"
#include "task.h"
#include "util.h"

#include <stdint.h>

/*****************************************************************************
* Definitions
******************************************************************************/

/** @brief Get the elapsed system time in milliseconds.

	@return The elapsed system time in milliseconds.
*/
uint32_t Kernel_GetElapsedSystemTimeMs();

/** @brief Get the number of completed execution cycles.

	For a cycle to be completed, all execution branches must have been walked.
	The duration of a cycle amounts to the minimal execution period multiplied by the number of branches.

	@return The number of completed execution cycles.
*/
uint32_t Kernel_GetCycleCount();

/** @brief Get the index of the currently active execution branch.

	@return The index of the currently active execution branch.
*/
uint32_t Kernel_GetBranchIndex();

/** @brief Initialize the kernel. */
void Kernel_Init();

/** @brief Start the kernel.

	@param [in] periodMs The minimal execution period for tasks.
*/
ErrorReasonStr_t Kernel_Start(uint32_t periodMs);

/** @brief Start a sporaric task.

	@param [in] pTask The sporadic task to start.
	@param [in] deadline The deadline for this task execution.
	@return Could the task be started?
*/
bool Kernel_StartSporadicTask(SporadicTask_t* pTask, uint32_t deadline);

/** @brief Start a aperiodic task.

	@param [in] pTask The aperiodic task to start.
	@return Could the task be started?
*/
bool Kernel_StartAperiodicTask(AperiodicTask_t* pTask);

/** @brief Create a new periodic task.

	@param [in] stackSize The size of the stack to use. The maximal number of item in the stack.
	@param [in] aStack The stack to use. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@param [in] periodExponent The exponent to measure the required period (`periodMs * 2^{periodExponent}`).
	@param [in] durationMs The required duration. Should be well below the the calculated period.
	@return Could the new task be created?
*/
bool Kernel_CreatePeriodicTask(
	size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName, uint8_t periodExponent, uint32_t durationMs);

/** @brief Create a new periodic task.

	@param [in] stackSize The size of the stack to use. The maximal number of item in the stack.
	@param [in] aStack The stack to use. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@param [in] periodExponent The exponent to measure the required period (`periodMs * 2^{periodExponent}`).
	@param [in] durationMs The required duration. Should be well below the the calculated period.
	@return The new task or `NULL`.
*/
AperiodicTask_t*
Kernel_CreateAperiodicTask(size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName, uint32_t priority);

/** @brief Create a new sporadic task.

	@param [in] stackSize The size of the stack for the new task.
	@param [in] aStack The stack for the new task. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@return The new task or `NULL`.
*/
SporadicTask_t* Kernel_CreateSporadicTask(size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName);

#endif // __KERNEL_H__
