#include "bit_list.h"

#include <stdio.h> //< Used for simple debugging with `printf`.
#include <string.h>

typedef struct BitPatterns {
	uint8_t aMasks[8];
	uint8_t aInvertedMasks[8];
	uint8_t aShifts[8];
} BitPatterns_t;

static uint8_t g_aShifts1[8]        = {0, 1, 2, 3, 4, 5, 6, 7};
static uint8_t g_aMasks1[8]         = {0b00000001, 0b00000010, 0b00000100, 0b00001000,
									   0b00010000, 0b00100000, 0b01000000, 0b10000000};
static uint8_t g_aInvertedMasks1[8] = {0b11111110, 0b11111101, 0b11111011, 0b11110111,
									   0b11101111, 0b11011111, 0b10111111, 0b01111111};

static uint8_t g_aShifts2[8]        = {0, 2, 4, 6, 0, 2, 4, 6};
static uint8_t g_aMasks2[8]         = {0b00000011, 0b00001100, 0b00110000, 0b11000000,
									   0b00000011, 0b00001100, 0b00110000, 0b11000000};
static uint8_t g_aInvertedMasks2[8] = {0b11111100, 0b11110011, 0b11001111, 0b00111111,
									   0b11111100, 0b11110011, 0b11001111, 0b00111111};

static uint8_t g_aShifts4[8]        = {0, 4, 0, 4, 0, 4, 0, 4};
static uint8_t g_aMasks4[8]         = {0b00001111, 0b11110000, 0b00001111, 0b11110000,
									   0b00001111, 0b11110000, 0b00001111, 0b11110000};
static uint8_t g_aInvertedMasks4[8] = {0b11110000, 0b00001111, 0b11110000, 0b00001111,
									   0b11110000, 0b00001111, 0b11110000, 0b00001111};

#define GET_BIT_SLICE(value, shift, mask) ((value & (mask)) >> (shift))

#define SET_BIT_SLICE(target, source, shift, mask, inverted_mask) \
	target = (target & (inverted_mask)) | (((source) << (shift)) & (mask))

/** @brief Initialize the list. */
void BitList_Init(BitList_t* pBitList, uint8_t itemsPerByte, BitListSize_t size, BitListSize_t len) {
	memcpy((void*)&pBitList->itemsPerByte, &itemsPerByte, sizeof(uint8_t));
	memcpy((void*)&pBitList->size, &size, sizeof(BitListSize_t));
	pBitList->length = len;
}

/** @brief Try to get an item in the list. */
static inline bool
BitList_TryGetItem(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem, uint8_t* aShifts, uint8_t* aMasks) {
	if (pos < pBitList->length) {
		BitListSize_t index = pos / pBitList->itemsPerByte;
		BitListSize_t rest  = pos % pBitList->itemsPerByte;
		*pItem              = GET_BIT_SLICE(pBitList->aData[index].byte, aShifts[rest], aMasks[rest]);
		return true;
	}
	return false;
}

/** @brief Try to get an item in the list (8 bits per item). */
bool BitList_TryGetItem8(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem) {
	if (pos < pBitList->length) {
		*pItem = pBitList->aData[pos].byte;
		return true;
	}
	return false;
}

/** @brief Try to get an item in the list (4 bits per item). */
bool BitList_TryGetItem4(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem) {
	return BitList_TryGetItem(pBitList, pos, pItem, g_aShifts4, g_aMasks4);
}

/** @brief Try to get an item in the list (2 bits per item). */
bool BitList_TryGetItem2(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem) {
	return BitList_TryGetItem(pBitList, pos, pItem, g_aShifts2, g_aMasks2);
}

/** @brief Try to get an item in the list (1 bit per item). */
bool BitList_TryGetItem1(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem) {
	return BitList_TryGetItem(pBitList, pos, pItem, g_aShifts1, g_aMasks1);
}

/** @brief Get an item in the list. */
static inline uint8_t BitList_GetItem(BitList_t* pBitList, BitListSize_t pos, uint8_t* aShifts, uint8_t* aMasks) {
	if (pos < pBitList->length) {
		BitListSize_t index = pos / pBitList->itemsPerByte;
		BitListSize_t rest  = pos % pBitList->itemsPerByte;
		return GET_BIT_SLICE(pBitList->aData[index].byte, aShifts[rest], aMasks[rest]);
	}
	return 0;
}

/** @brief Get an item in the list (8 bits per item). */
uint8_t BitList_GetItem8(BitList_t* pBitList, BitListSize_t pos) {
	if (pos < pBitList->length) { return pBitList->aData[pos].byte; }
	return 0;
}

/** @brief Get an item in the list (4 bits per item). */
uint8_t BitList_GetItem4(BitList_t* pBitList, BitListSize_t pos) {
	return BitList_GetItem(pBitList, pos, g_aShifts4, g_aMasks4);
}

/** @brief Get an item in the list (2 bits per item). */
uint8_t BitList_GetItem2(BitList_t* pBitList, BitListSize_t pos) {
	return BitList_GetItem(pBitList, pos, g_aShifts2, g_aMasks2);
}

/** @brief Get an item in the list (1 bit per item). */
uint8_t BitList_GetItem1(BitList_t* pBitList, BitListSize_t pos) {
	return BitList_GetItem(pBitList, pos, g_aShifts1, g_aMasks1);
}

typedef uint8_t (*GetItemFn_t)(BitList_t* pBitList, BitListSize_t pos);

/** @brief Set an item in the list the given value. */
static inline void BitList_SetItem(
	BitList_t* pBitList, BitListSize_t pos, uint8_t item, uint8_t itemsPerByte, uint8_t* aShifts, uint8_t* aMasks,
	uint8_t* aInvertedMasks) {
	BitListSize_t index = pos / itemsPerByte;
	BitListSize_t rest  = pos % itemsPerByte;
	SET_BIT_SLICE(pBitList->aData[index].byte, item, aShifts[rest], aMasks[rest], aInvertedMasks[rest]);
}

/** @brief Set an item in the list the given value (8 bits per item). */
void BitList_SetItem8(BitList_t* pBitList, BitListSize_t pos, uint8_t item) { pBitList->aData[pos].byte = item; }

/** @brief Set an item in the list the given value (4 bits per item). */
void BitList_SetItem4(BitList_t* pBitList, BitListSize_t pos, uint8_t item) {
	BitList_SetItem(pBitList, pos, item, 2, g_aShifts4, g_aMasks4, g_aInvertedMasks4);
}

/** @brief Set an item in the list the given value (2 bits per item). */
void BitList_SetItem2(BitList_t* pBitList, BitListSize_t pos, uint8_t item) {
	BitList_SetItem(pBitList, pos, item, 4, g_aShifts2, g_aMasks2, g_aInvertedMasks2);
}

/** @brief Set an item in the list the given value (1 bit per item). */
void BitList_SetItem1(BitList_t* pBitList, BitListSize_t pos, uint8_t item) {
	BitList_SetItem(pBitList, pos, item, 8, g_aShifts1, g_aMasks1, g_aInvertedMasks1);
}

typedef void (*SetItemFn_t)(BitList_t* pBitList, BitListSize_t pos, uint8_t item);

/** @brief Append the list by the given value. */
static inline bool BitList_AppendItem(BitList_t* pBitList, uint8_t item, SetItemFn_t setItem) {
	BitListSize_t minsize = pBitList->length + 1;
	if (minsize > pBitList->size) { return false; }
	setItem(pBitList, pBitList->length, item);
	pBitList->length++;
	return true;
}

/** @brief Append the list by the given value (8 bits per item). */
bool BitList_AppendItem8(BitList_t* pBitList, uint8_t item) {
	return BitList_AppendItem(pBitList, item, BitList_SetItem8);
}

/** @brief Append the list by the given value (4 bits per item). */
bool BitList_AppendItem4(BitList_t* pBitList, uint8_t item) {
	return BitList_AppendItem(pBitList, item, BitList_SetItem4);
}

/** @brief Append the list by the given value (2 bits per item). */
bool BitList_AppendItem2(BitList_t* pBitList, uint8_t item) {
	return BitList_AppendItem(pBitList, item, BitList_SetItem2);
}

/** @brief Append the list by the given value (1 bit per item). */
bool BitList_AppendItem1(BitList_t* pBitList, uint8_t item) {
	return BitList_AppendItem(pBitList, item, BitList_SetItem1);
}

/** @brief Find the index of an item by a predicate. */
static inline BitListSize_t
BitList_FindIndex(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure, GetItemFn_t getItem) {
	BitListSize_t pos;
	uint8_t       item;

	for (pos = 0; pos < pBitList->length; pos++) {
		item = getItem(pBitList, pos);
		if (fPredicate(&item, pClosure)) { return pos; }
	}

	return -1;
}

/** @brief Find the index of an item by a predicate (8 bits per item). */
BitListSize_t BitList_FindIndex8(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure) {
	return BitList_FindIndex(pBitList, fPredicate, pClosure, BitList_GetItem8);
}

/** @brief Find the index of an item by a predicate (4 bits per item). */
BitListSize_t BitList_FindIndex4(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure) {
	return BitList_FindIndex(pBitList, fPredicate, pClosure, BitList_GetItem4);
}

/** @brief Find the index of an item by a predicate (4 bits per item). */
BitListSize_t BitList_FindIndex2(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure) {
	return BitList_FindIndex(pBitList, fPredicate, pClosure, BitList_GetItem2);
}

/** @brief Find the index of an item by a predicate (1 bit per item). */
BitListSize_t BitList_FindIndex1(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure) {
	return BitList_FindIndex(pBitList, fPredicate, pClosure, BitList_GetItem1);
}
