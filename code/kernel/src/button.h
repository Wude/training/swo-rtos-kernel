#ifndef __BUTTON__
#define __BUTTON__

#include "util.h"

#include "stm32f4xx.h"

/** @brief A button using a bool GPIO pin. */
typedef struct Button {
	uint32_t      currentPhaseDurationMs; /**< The tick duration of the current phase. */
	uint32_t      lastPhaseDurationMs;    /**< The tick duration of the last phase. */
	uint32_t      lastElapsedTimeMs;      /**< The number of ticks when the last change happened. */
	GPIO_TypeDef* pPort;                  /**< The port of the pin to handle. */
	short         pin;                    /**< The number of the pin to handle. */
	bool          normallyClosed;         /**< Is the pin "normally closed"? */
	bool          lastValue;              /**< The last value fetched from the pin. */
	bool          changing;               /**< Is the pin currently changing? */
	bool          locked;                 /**< Is the current phase locked? */
} Button_t;

/** @brief Initialize a value of type "Button_t". */
void Button_Init(Button_t* pPin, GPIO_TypeDef* port, short pin, bool normallyClosed);

/** @brief Update the pin status information. */
void Button_Update(Button_t* pPin);

/** @brief Is the pin currently "active"? */
bool Button_Active(Button_t* pPin);

/** @brief Is the pin activating - currently changing from "inactive" to "active"? */
bool Button_Activating(Button_t* pPin);

/** @brief Is the pin deactivating - currently changing from "active" to "inactive"? */
bool Button_Deactivating(Button_t* pPin);

/** @brief Is the pin deactivating after having been active for less than a certain duration? */
bool Button_DeactivatingBeforeTime(Button_t* pPin, uint32_t duration);

/** @brief Does the time of the pin being active exceed a certain duration? */
bool Button_ActiveForSomeTime(Button_t* pPin, uint32_t duration, bool locking);

#endif // __BUTTON__
