#include "kernel.h"

#include "scheduling_tree.h"

#include "misc.h"
#include "stm32f4xx.h"

#include "core_cm4.h"

#include <inttypes.h>
#include <stdint.h>
#include <string.h>

/*****************************************************************************
* Private Types
******************************************************************************/

/** @brief The service call codes.

	Supervisor calls with the following codes should never return: `ServiceCode_StartKernel`, `ServiceCode_Yield`
*/
typedef enum ServiceCode {
	ServiceCode_StartKernel        = 0, //< Start the kernel.
	ServiceCode_Yield              = 1, //< Yield the remaining execution time. Only for normal threads.
	ServiceCode_StartSporadicTask  = 2, //< Start a sporadic task.
	ServiceCode_StartAperiodicTask = 3, //< Start a aperiodic task.
} ServiceCode_t;

/** @brief A task control block. */
typedef struct TaskControlBlock {
	volatile uint32_t* pStack;            //< The current stack pointer.
	uint64_t*          pStackLower;       //< The lower address of the stack.
	uint64_t*          pStackUpper;       //< The upper address just outside of the stack.
	ActionFn_t         fAction;           //< The action to execute as task workload.
	Task_t*            pTask;             //< The task description.
	volatile uint32_t  elapsedDurationMs; //< The elapsed duration for the current task run.
	volatile bool      isRunning;         //< Is the task running? It may also be suspended!
} TaskControlBlock_t;

/*****************************************************************************
* Private declarations.
******************************************************************************/

/** @brief Yield the remaining execution time. Only for normal threads. */
void Kernel_Yield();

/*****************************************************************************
* Private definitions.
******************************************************************************/

#ifdef DEBUG
/** @brief This define reduces leftovers of previous exeutions from stack frames. */
#define STACK_FRAME_WRITE_ZEROS
#endif

#define KERNEL_ALREADY_STARTED "Kernel has already been started"

/** @brief Has the kernel been started. */
static volatile bool g_started = false;

/** @brief The time elapsed while the system has been running. */
static volatile uint32_t g_elapsedSystemTimeMs = 0;

#define TASK_CONTROL_BLOCK_LIST_SIZE 96
static size_t             g_taskControlBlockCount = 0;
static TaskControlBlock_t g_aTaskControlBlocks[TASK_CONTROL_BLOCK_LIST_SIZE];

#define IDLE_TIME_STACK_SIZE 64
static uint64_t           g_aIdleStack[IDLE_TIME_STACK_SIZE]; //< The stack used for idle time.
static TaskControlBlock_t g_idleTaskControlBlock;             //< The control block for idle time.

/** @brief The control block of the currently active task. */
static TaskControlBlock_t* volatile g_pCurrentTaskControlBlock = &g_idleTaskControlBlock;

/** @brief The control block of the task that becomes active on the next switch. */
static TaskControlBlock_t* volatile g_pNextTaskControlBlock = &g_idleTaskControlBlock;

static volatile uint32_t g_lastTaskSwitchTimeMs = 0; //< The timestamp of the last task switch.
static volatile uint32_t g_nextTaskSwitchTimeMs = 0; //< The timestamp of the next task switch.

static PeriodicTaskList_t  g_periodicTasks;  //< The list of periodic tasks.
static AperiodicTaskList_t g_aperiodicTasks; //< The list of aperiodic tasks.
static SporadicTaskList_t  g_sporadicTasks;  //< The list of sporadic tasks.

static SchedulingTree_t         g_schedulingTree;     //< The tree used for planning the periodic tasks.
static SchedulingTreeIterator_t g_schedulingIterator; //< The iterator used for walking the execution branches.
static AperiodicTaskChain_t     g_aperiodicTaskChain; //< The chain of aperiodic tasks by priority.
static SporadicTaskChain_t      g_sporadicTaskChain;  //< The chain of sporadic tasks by deadline.

/** @brief Get a supervisor call result. */
#define GET_SVC_RESULT(result) asm volatile("MOV %0, R0" : "=r"(result))

/** @brief Call the supervisor (IRQ: SVC). */
static inline __attribute__((always_inline)) void SVC(ServiceCode_t serviceCode) {
	asm volatile("SVC %[value]" ::[value] "I"((uint8_t)serviceCode));
}

/** @brief Call a pendable service (IRQ: PendSV). */
static inline void PendSV() { SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk; }

/** @brief Is the current execution in thread mode?

	@return Is the current execution in thread mode?s
*/
static inline bool InThreadMode() { return (SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) == 0; }

/** @brief Prepare a stack.

	Prepare a "Full Descending" stack:
	- The first stack element can be found at just below the highest address.
	- The stack pointer has to be decremented before copying the stack element.

	@param [in] pTaskControlBlock The task control block referencing the stack.
*/
static void PrepareTaskStack(TaskControlBlock_t* pTaskControlBlock) {
	pTaskControlBlock->pStack = (uint32_t*)pTaskControlBlock->pStackUpper;

	CallerStackFrame_t* pCallerStackFrame = PUSH_FRAME_FD(pTaskControlBlock->pStack, CallerStackFrame_t);
	CalleeStackFrame_t* pCalleeStackFrame = PUSH_FRAME_FD(pTaskControlBlock->pStack, CalleeStackFrame_t);

	pCallerStackFrame->xPSR = XPSR_RESET_VALUE;
	pCallerStackFrame->PC   = (uint32_t)pTaskControlBlock->fAction;
	pCallerStackFrame->LR   = (uint32_t)Kernel_Yield;
#ifdef STACK_FRAME_WRITE_ZEROS
	pCallerStackFrame->R12 = 0;
	pCallerStackFrame->R3  = 0;
	pCallerStackFrame->R2  = 0;
	pCallerStackFrame->R1  = 0;
	pCallerStackFrame->R0  = 0;
#endif
	pCalleeStackFrame->LR = RETURN_TO_THREAD_MODE_PSP;
#ifdef STACK_FRAME_WRITE_ZEROS
	pCalleeStackFrame->R11 = 0;
	pCalleeStackFrame->R10 = 0;
	pCalleeStackFrame->R9  = 0;
	pCalleeStackFrame->R8  = 0;
	pCalleeStackFrame->R7  = 0;
	pCalleeStackFrame->R6  = 0;
	pCalleeStackFrame->R5  = 0;
	pCalleeStackFrame->R4  = 0;
#endif
}

/** @brief Initialize a task control block.

	@param [in] pTaskControlBlock The task control block to initialize.
	@param [in] stackSize The size of the stack for the new task.
	@param [in] aStack The stack for the new task. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] isRunning Is the task running?
	@param [in] pTask The task description.
*/
static void TaskControlBlock_Init(
	TaskControlBlock_t* pTaskControlBlock, size_t stackSize, uint64_t* aStack, ActionFn_t fAction, bool isRunning,
	Task_t* pTask) {
	pTaskControlBlock->pStack            = NULL;
	pTaskControlBlock->pStackLower       = aStack;
	pTaskControlBlock->pStackUpper       = aStack + stackSize;
	pTaskControlBlock->fAction           = fAction;
	pTaskControlBlock->pTask             = pTask;
	pTaskControlBlock->elapsedDurationMs = 0;
	pTaskControlBlock->isRunning         = isRunning;
	pTask->pControlBlock                 = pTaskControlBlock;
	PrepareTaskStack(pTaskControlBlock);
}

/** @brief Discard the currently running task. */
static void DiscardCurrentTask() {
	// Re-schedule non-periodic tasks that did not yet return by themselfes.
	if (g_pCurrentTaskControlBlock->pTask != NULL && g_pCurrentTaskControlBlock->isRunning) {
		if (g_pCurrentTaskControlBlock->pTask->taskType == TaskType_Sporadic) {
			SporadicTask_t* pSporadicTask = (SporadicTask_t*)g_pCurrentTaskControlBlock->pTask;
			// Reinsert the sporadic task by its deadline.
			SporadicTaskChain_InsertFromFront(
				&g_sporadicTaskChain, pSporadicTask, (CompareFn_t)SporadicTask_Compare, true);
		} else if (g_pCurrentTaskControlBlock->pTask->taskType == TaskType_Aperiodic) {
			AperiodicTask_t* pAperiodicTask = (AperiodicTask_t*)g_pCurrentTaskControlBlock->pTask;
			// Reinsert the aperiodic task after other tasks with the same priority.
			AperiodicTaskChain_InsertFromBehind(
				&g_aperiodicTaskChain, pAperiodicTask, (CompareFn_t)AperiodicTask_Compare, true);
		}
	}
}

/** @brief Select the next task among the non-periodic tasks. A new task will be set in any case.

	@param [in] freeDurationMs Free execution duration.
*/
static void SelectNextTaskNonPeriodic(uint32_t freeDurationMs) {
	// What is the next sporadic task to execute?
	// Sporadic tasks are sorted by their deadline beginning with the lowest.
	// Aperiodic tasks are always prioritized lower.
	SporadicTask_t* pSporadicTask = SporadicTaskChain_GetItemNullable(g_sporadicTaskChain.pFirst);
	if (pSporadicTask != NULL) {
		SporadicTaskChain_Remove(&g_sporadicTaskChain, pSporadicTask);
		g_pNextTaskControlBlock = pSporadicTask->base.pControlBlock;
	} else {
		// What is the next aperiodic task to execute?
		// Aperiodic tasks are sorted by their priority beginning with the highest (numerically lowest).
		AperiodicTask_t* pAperiodicTask = AperiodicTaskChain_GetItemNullable(g_aperiodicTaskChain.pFirst);
		if (pAperiodicTask != NULL) {
			AperiodicTaskChain_Remove(&g_aperiodicTaskChain, pAperiodicTask);
			g_pNextTaskControlBlock = pAperiodicTask->base.pControlBlock;
		} else {
			// No aperiodic or sporadic task to select from. Instate the idle task.
			g_pNextTaskControlBlock = &g_idleTaskControlBlock;
		}
	}
	g_lastTaskSwitchTimeMs = g_elapsedSystemTimeMs;
	g_nextTaskSwitchTimeMs = g_lastTaskSwitchTimeMs + freeDurationMs;
}

/** @brief Select the next task. A new task will be set in any case. */
static void SelectNextTask() {
	SchedulingTreeSlot_t* pSlot = SchedulingTreeIterator_Next(&g_schedulingIterator);
	if (pSlot->pPeriodicTask != NULL) {
		g_pNextTaskControlBlock = pSlot->pPeriodicTask->base.pControlBlock;
		g_lastTaskSwitchTimeMs  = g_elapsedSystemTimeMs;
		g_nextTaskSwitchTimeMs  = g_lastTaskSwitchTimeMs + pSlot->pPeriodicTask->durationMs;

		if (!g_pNextTaskControlBlock->isRunning) {
			// A periodic task ended before suspension. It has to be prepared for its next execution.
			g_pNextTaskControlBlock->isRunning = true;
			PrepareTaskStack(g_pNextTaskControlBlock);
		}
	} else {
		SelectNextTaskNonPeriodic(pSlot->freeDurationMs);
	}
}

/** @brief Prepare the first task for execution. */
static __attribute__((naked)) void PrepareFirstTask() {
	// Get the current task control block.
	asm volatile("LDR       R1,     =g_pCurrentTaskControlBlock"); // `R1 = &g_pCurrentTaskControlBlock`

	// The stack pointer is the first member in a task control block.
	// That means `R3` is a pointer to the stack pointer and a pointer to the task control block.
	asm volatile("LDR       R3,     [R1]"); // `R3 = (uint32_t**)(*(&g_pCurrentTaskControlBlock))`.

	// ----------------------------------------
	asm volatile("LDR       R0,     [R3]");         // Load the stack pointer of the next task.
	asm volatile("LDMIA     R0!,    {R4-R11, LR}"); // Load the basic callee stack frame of the next task.

	// ----------------------------------------
	asm volatile("MSR       PSP,    R0"); // Prepare "Process Stack Pointer"
	asm volatile("DSB");
	asm volatile("ISB");

	// asm volatile("CPSIE     I");
	asm volatile("BX        LR");
}

/** @brief Yield the remaining execution time. Only for normal threads. */
static void Yield() {
	// There has to be a task since the idle thread should not yield.
	if (g_pCurrentTaskControlBlock->pTask != NULL) {
		g_pCurrentTaskControlBlock->isRunning = false; // The task simply ends.
		__disable_irq();
		SelectNextTaskNonPeriodic(g_nextTaskSwitchTimeMs - g_elapsedSystemTimeMs);
		PendSV();
		__enable_irq();
	}
}

/** @brief Start a sporadic task.

	@param [in] pTask The sporadic task to start.
	@param [in] deadline The deadline for this task execution.
*/
static bool StartSporadicTask(SporadicTask_t* pSporadicTask, uint32_t deadline) {
	TaskControlBlock_t* pTaskControlBlock = pSporadicTask->base.pControlBlock;
	if (pTaskControlBlock->isRunning) { return false; }
	pTaskControlBlock->isRunning = true;
	pSporadicTask->deadline      = deadline;
	SporadicTaskChain_InsertFromFront(&g_sporadicTaskChain, pSporadicTask, (CompareFn_t)SporadicTask_Compare, true);
	return true;
}

/** @brief Start a aperiodic task.

	@param [in] pTask The aperiodic task to start.
*/
static bool StartAperiodicTask(AperiodicTask_t* pAperiodicTask) {
	TaskControlBlock_t* pTaskControlBlock = pAperiodicTask->base.pControlBlock;
	if (pTaskControlBlock->isRunning) { return false; }
	pTaskControlBlock->isRunning = true;
	AperiodicTaskChain_InsertFromBehind(
		&g_aperiodicTaskChain, pAperiodicTask, (CompareFn_t)AperiodicTask_Compare, true);
	return true;
}

/*****************************************************************************
* Definitions.
******************************************************************************/

/** @brief The pendable service call handler (IRQ: PendSV).

	Will switch the current task when there is a next task.
*/
__attribute__((naked)) void PendSV_Handler() {
	// asm volatile("CPSID     I");

	// Get the current and next task control block.
	asm volatile("LDR       R1,     =g_pCurrentTaskControlBlock"); // `R1 = &g_pCurrentTaskControlBlock`
	asm volatile("LDR       R2,     =g_pNextTaskControlBlock");    // `R2 = &g_pNextTaskControlBlock`

	// The stack pointer is the first member in a task control block.
	// That means `R3` is a pointer to the stack pointer and a pointer to the task control block.
	asm volatile("LDR       R3,     [R1]"); // `R3 = (uint32_t**)(*(&g_pCurrentTaskControlBlock))`.
	asm volatile("LDR       R0,     [R2]"); // `R0 = (uint32_t**)(*(&g_pNextTaskControlBlock))`.

	// End here if current and next task are the same. This should not normally be the case.
	asm volatile("CMP       R3,     R0");
	asm volatile("BEQ       SkipContextSwitch");

	asm volatile("MRS       R0,     PSP"); // Get the "Process Stack Pointer"
	asm volatile("DSB");
	asm volatile("ISB");

	// ----------------------------------------
	// In case the Floating-Point Unit was used, store the FPU callee stack frame.
	asm volatile("TST       LR,     0x10");
	asm volatile("IT        EQ");
	asm volatile("VSTMDBEQ  R0!,    {S16-S31}");

	asm volatile("STMDB     R0!,    {R4-R11, LR}"); // Store the basic callee stack frame.
	asm volatile("STR       R0,     [R3]");         // Store the modified stack pointer into the current task.

	// ----------------------------------------
	// The next task becomes the current task.
	asm volatile("LDR       R3,     [R2]"); // `R3 = (uint32_t**)(*(&g_pNextTaskControlBlock))`.
	asm volatile("STR       R3,     [R1]"); // `(*(&g_pCurrentTaskControlBlock)) = (TaskControlBlock_t*)R3`

	// ----------------------------------------
	asm volatile("LDR       R0,     [R3]");         // Load the stack pointer of the next task.
	asm volatile("LDMIA     R0!,    {R4-R11, LR}"); // Load the basic callee stack frame of the next task.

	// In case the Floating-Point Unit was used, load the FPU callee stack frame.
	asm volatile("TST       LR,     0x10");
	asm volatile("IT        EQ");
	asm volatile("VLDMIAEQ  R0!,    {S16-S31}");

	// ----------------------------------------
	asm volatile("MSR       PSP,    R0"); // Prepare "Process Stack Pointer"
	asm volatile("DSB");
	asm volatile("ISB");

	asm volatile("SkipContextSwitch:");
	// asm volatile("CPSIE     I");
	asm volatile("BX        LR");
}

/** @brief The supervisor call handler (IRQ: SVC). */
__attribute__((naked)) void SVC_Handler(void) {
	asm volatile("TST   LR, 0b100");        // EXC_RETURN indicates which stack is used.
	asm volatile("ITE   EQ");               // If Equal Then Else.
	asm volatile("MRSEQ R1, MSP");          // `0` -> Store MSP.
	asm volatile("MRSNE R1, PSP");          // `1` -> Store PSP.
	asm volatile("LDR   R0, [R1, 24]");     // Get the PC inside the caller stack frame.
	asm volatile("LDRB  R0, [R0, -2]");     // Get the service code from inside the SVC instruction.
	asm volatile("B SVC_Handler_Internal"); // R0 and R1 are the call arguments.
}

/** @brief The internal supervisor call handler (IRQ: SVC).

	@param [in] serviceCode The service number for this supervisor call.
	@param [in] pCallerStackFrame The stack frame of the caller. Can be used for arguments and a result.
*/
void SVC_Handler_Internal(ServiceCode_t serviceCode, CallerStackFrame_t* pCallerStackFrame) {
	switch (serviceCode) {
	case ServiceCode_StartKernel: PrepareFirstTask(); break;
	case ServiceCode_Yield: Yield(); break;
	case ServiceCode_StartSporadicTask:
		pCallerStackFrame->R0 =
			StartSporadicTask((SporadicTask_t*)(intptr_t)pCallerStackFrame->R0, pCallerStackFrame->R1);
		break;
	case ServiceCode_StartAperiodicTask:
		pCallerStackFrame->R0 = StartAperiodicTask((AperiodicTask_t*)(intptr_t)pCallerStackFrame->R0);
		break;
	}
}

/** @brief The system tick handler (IRQ: SysTick). */
void SysTick_Handler() {
	// Requirements:
	// - Expected to be called every millisecond.
	// - Interrupt priority should be high (numerically low).
	++g_elapsedSystemTimeMs;
	++g_pCurrentTaskControlBlock->elapsedDurationMs;

	if (g_started && g_elapsedSystemTimeMs >= g_nextTaskSwitchTimeMs) {
		__disable_irq();
		DiscardCurrentTask();
		SelectNextTask();
		PendSV();
		__enable_irq();
	}
}

/** @brief The idle thread. */
void Kernel_Idle() {
	for (;;) { }
}

/** @brief Get the elapsed system time in milliseconds.

	@return The elapsed system time in milliseconds.
*/
uint32_t Kernel_GetElapsedSystemTimeMs() { return g_elapsedSystemTimeMs; }

/** @brief Get the number of completed execution cycles.

	For a cycle to be completed, all execution branches must have been walked.
	The duration of a cycle amounts to the minimal execution period multiplied by the number of branches.

	@return The number of completed execution cycles.
*/
uint32_t Kernel_GetCycleCount() { return g_schedulingIterator.cycleCount; }

/** @brief Get the index of the currently active execution branch.

	@return The index of the currently active execution branch.
*/
uint32_t Kernel_GetBranchIndex() { return g_schedulingIterator.branchIndex; }

/** @brief Initialize the kernel. */
void Kernel_Init() {
	TaskControlBlock_Init(&g_idleTaskControlBlock, IDLE_TIME_STACK_SIZE, g_aIdleStack, Kernel_Idle, true, NULL);

	PeriodicTaskList_Init(&g_periodicTasks, 0);
	SporadicTaskList_Init(&g_sporadicTasks, 0);
	AperiodicTaskList_Init(&g_aperiodicTasks, 0);

	SporadicTaskChain_Init(&g_sporadicTaskChain);
	AperiodicTaskChain_Init(&g_aperiodicTaskChain);

	NVIC_SetPriority(SysTick_IRQn, 0);
	NVIC_SetPriority(SVCall_IRQn, 15);
	NVIC_SetPriority(PendSV_IRQn, 15);

	// Generate a "SysTick" interrupt every millisecond.
	SysTick_Config(SystemCoreClock / 1000);
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

	SCB->CPACR |= 0xF << 20; // Enable the FPU
	__DSB();
	__ISB();
}

/** @brief Start the kernel.

	@param [in] periodMs The minimal execution period for tasks.
*/
ErrorReasonStr_t Kernel_Start(uint32_t periodMs) {
	if (g_started) { return KERNEL_ALREADY_STARTED; }
	ErrorReasonStr_t sReason = SchedulingTree_Init(&g_schedulingTree, periodMs, &g_periodicTasks);
	if (sReason != NULL) { return sReason; }

	// We select the first task for execution.
	SchedulingTreeSlot_t* pSlot = SchedulingTreeIterator_Init(&g_schedulingIterator, &g_schedulingTree);
	if (pSlot->pPeriodicTask != NULL) {
		g_pNextTaskControlBlock    = pSlot->pPeriodicTask->base.pControlBlock;
		g_pCurrentTaskControlBlock = g_pNextTaskControlBlock;
		g_lastTaskSwitchTimeMs     = g_elapsedSystemTimeMs;
		g_nextTaskSwitchTimeMs     = g_lastTaskSwitchTimeMs + pSlot->pPeriodicTask->durationMs;
	} else {
		// Although unlikely at this point, we look for non-periodic tasks.
		SelectNextTaskNonPeriodic(pSlot->freeDurationMs);
	}

	g_started = true;
	SVC(ServiceCode_StartKernel);

	// Switch to the "Process Stack Pointer" of the current task.
	// The double-cast is in place to avoid a linting error on a 64-bit dev host.
	__set_PSP((uint32_t)(intptr_t)g_pCurrentTaskControlBlock->pStack);
	__set_CONTROL(CONTROL_THREAD_MODE_PSP);
	__DSB();
	__ISB();

	return NULL; // This function should never actually return.
}

/** @brief Yield the remaining execution time. Only for normal threads. */
void Kernel_Yield() {
	// There should be no currently running interrupt service routine. Only a normal thread may yield.
	if (InThreadMode()) { SVC(ServiceCode_Yield); }
}

/** @brief Start a sporadic task.

	@param [in] pTask The sporadic task to start.
	@param [in] deadline The deadline for this task execution.
	@return Could the task be started?
*/
bool Kernel_StartSporadicTask(SporadicTask_t* pTask, uint32_t deadline) {
	bool success;
	SVC(ServiceCode_StartSporadicTask);
	GET_SVC_RESULT(success);
	return success;
}

/** @brief Start a aperiodic task.

	@param [in] pTask The aperiodic task to start.
	@return Could the task be started?
*/
bool Kernel_StartAperiodicTask(AperiodicTask_t* pTask) {
	bool success;
	SVC(ServiceCode_StartAperiodicTask);
	GET_SVC_RESULT(success);
	return success;
}

/** @brief Create a new periodic task.

	@param [in] stackSize The size of the stack to use. The maximal number of item in the stack.
	@param [in] aStack The stack to use. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@param [in] periodExponent The exponent to measure the required period (`periodMs * 2^{periodExponent}`).
	@param [in] durationMs The required duration. Should be well below the the calculated period.
	@return Could the new task be created?
*/
bool Kernel_CreatePeriodicTask(
	size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName, uint8_t periodExponent, uint32_t durationMs) {
	if (!g_started && g_taskControlBlockCount < TASK_CONTROL_BLOCK_LIST_SIZE) {
		PeriodicTask_t* pPeriodicTask = PeriodicTaskList_AppendItemEmpty(&g_periodicTasks);

		if (pPeriodicTask != NULL) {
			PeriodicTask_Init(pPeriodicTask, sName, periodExponent, durationMs);
			TaskControlBlock_Init(
				&g_aTaskControlBlocks[g_taskControlBlockCount++], stackSize, aStack, fAction, true,
				&pPeriodicTask->base);

			return true;
		}
	}

	return false;
}

/** @brief Create a new aperiodic task.

	@param [in] stackSize The size of the stack for the new task.
	@param [in] aStack The stack for the new task. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@param [in] priority The priority of the new aperiodic task.
	@return The new task or `NULL`.
*/
AperiodicTask_t*
Kernel_CreateAperiodicTask(size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName, uint32_t priority) {
	if (!g_started && g_taskControlBlockCount < TASK_CONTROL_BLOCK_LIST_SIZE) {
		AperiodicTask_t* pAperiodicTask = AperiodicTaskList_AppendItemEmpty(&g_aperiodicTasks);

		if (pAperiodicTask != NULL) {
			AperiodicTask_Init(pAperiodicTask, sName, priority);
			TaskControlBlock_Init(
				&g_aTaskControlBlocks[g_taskControlBlockCount++], stackSize, aStack, fAction, false,
				&pAperiodicTask->base);

			return (AperiodicTask_t*)pAperiodicTask;
		}
	}

	return NULL;
}

/** @brief Create a new sporadic task.

	@param [in] stackSize The size of the stack for the new task.
	@param [in] aStack The stack for the new task. An array of 32-bit unsigned integers.
	@param [in] fAction The action to call when executing the new task.
	@param [in] sName The name of the task.
	@return The new task or `NULL`.
*/
SporadicTask_t* Kernel_CreateSporadicTask(size_t stackSize, uint64_t* aStack, ActionFn_t fAction, Str_t sName) {
	if (!g_started && g_taskControlBlockCount < TASK_CONTROL_BLOCK_LIST_SIZE) {
		SporadicTask_t* pSporadicTask = SporadicTaskList_AppendItemEmpty(&g_sporadicTasks);

		if (pSporadicTask != NULL) {
			SporadicTask_Init(pSporadicTask, sName);
			TaskControlBlock_Init(
				&g_aTaskControlBlocks[g_taskControlBlockCount++], stackSize, aStack, fAction, false,
				&pSporadicTask->base);

			return (SporadicTask_t*)pSporadicTask;
		}
	}
	return NULL;
}
