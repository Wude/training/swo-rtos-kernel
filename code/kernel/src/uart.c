#include "uart.h"

#include "itm.h"
#include "util.h"

#include "misc.h"
#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_usart.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

/*****************************************************************************
* Private definitions.
******************************************************************************/

DEFINE_BUFFER(CharBuffer, CharBuffer_t, char, 128)

static CharBuffer_t g_RxBuffer = {0};
static CharBuffer_t g_TxBuffer = {0};

static inline void USART_ConfigAsyncMode(USART_TypeDef* USARTx) {
	/* In Asynchronous mode, the following bits must be kept cleared:
	- LINEN, CLKEN bits in the USART_CR2 register,
	- SCEN, IREN and HDSEL bits in the USART_CR3 register.*/
	CLEAR_BIT(USARTx->CR2, (USART_CR2_LINEN | USART_CR2_CLKEN));
	CLEAR_BIT(USARTx->CR3, (USART_CR3_SCEN | USART_CR3_IREN | USART_CR3_HDSEL));
}

/*****************************************************************************
* Definitions.
******************************************************************************/

/** @brief Initialize USART2. */
void UART_Init() {
	// NVIC_SetPriority(USART2_IRQn, 1);

	USART_InitTypeDef uart = {0};
	GPIO_InitTypeDef  gpio = {0};
	NVIC_InitTypeDef  nvic = {0};

	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin   = GPIO_Pin_2 | GPIO_Pin_3;
	gpio.GPIO_Mode  = GPIO_Mode_AF;
	gpio.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOA, &gpio);

	USART_StructInit(&uart);
	uart.USART_Mode       = USART_Mode_Rx | USART_Mode_Tx;
	uart.USART_BaudRate   = 115200;
	uart.USART_StopBits   = USART_StopBits_1;
	uart.USART_WordLength = USART_WordLength_8b;
	uart.USART_Parity     = USART_Parity_No;
	USART_Init(USART2, &uart);

	nvic.NVIC_IRQChannel                   = USART2_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 5;
	nvic.NVIC_IRQChannelCmd                = ENABLE;
	NVIC_Init(&nvic);

	USART_ConfigAsyncMode(USART2);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART2, ENABLE);
}

/** @brief The USART2 interrupt request handler (IRQ: USART2). */
void USART2_IRQHandler() {
	if (USART_GetITStatus(USART2, USART_IT_RXNE) == SET) {
		char c = USART_ReceiveData(USART2);
		CharBuffer_Write(&g_RxBuffer, c);
	}

	if (USART_GetITStatus(USART2, USART_IT_TXE) == SET) {
		if (CharBuffer_GetCount(&g_TxBuffer) > 0) {
			char c = CharBuffer_Read(&g_TxBuffer);
			USART_SendData(USART2, c);
		} else {
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
		}
	}
}

/** @brief Get the number of characters to receive over USART2.

	@return The number of characters to receive.
*/
BufferSize_t UART_GetCount() { return CharBuffer_GetCount(&g_RxBuffer); }

/** @brief Receive a character over USART2.

	@param [in] c The character to receive.
*/
char UART_ReceiveChar() {
	USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
	char c = CharBuffer_Read(&g_RxBuffer);
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	return c;
}

/** @brief Send a character over USART2.

	@param [in] c The character to send.
*/
void UART_SendChar(char c) {
	ITM_SendChar(c); // Duplicate all characters through ITM.

	while (CharBuffer_GetCount(&g_TxBuffer) >= CharBuffer_Size()) { }

	USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
	CharBuffer_Write(&g_TxBuffer, c);
	USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
}

/** @brief Send data over USART2.

	@param [in] aData The data to send.
	@param [in] count The number of characters to send.
*/
void UART_SendData(const char* aData, size_t count) {
	if (aData != NULL) {
		for (size_t i = 0; i < count; ++i) { UART_SendChar(aData[i]); }
	}
}

void UART_SendString(Str_t sText) {
	if (sText != NULL) {
		while (*sText != '\0') { UART_SendChar(*sText++); }
	}
}

void UART_SendUint32(uint32_t value) {
	size_t        count = 10;
	volatile char aChars[count + 1];

	count = sprintf((char*)aChars, "%" PRIu32, value);
	for (size_t i = 0; i < count; ++i) { UART_SendChar(aChars[i]); }
}
