#ifndef __UTIL__
#define __UTIL__

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define EMTPY

#ifndef PRAGMA
#define PRAGMA(value) _Pragma(#value)
#endif

#ifndef PRAGMA_PACK
#define PRAGMA_PACK(value) PRAGMA(pack(value))
#endif

#define MAX(a, b) (((a) > (b)) ? (a) : (b))
#define MIN(a, b) (((a) < (b)) ? (a) : (b))

#define CEIL_MULTIPLE_OF_TWO(n)  ((((n) + 1) | 0b1) - 1)
#define CEIL_MULTIPLE_OF_FOUR(n) ((((n) + 1) | 0b10) - 1)

#define DEFINE_HANDLE_SPEC(handle_type_name, handle_type) \
	typedef struct handle_type_name { \
		void* pContent; \
	} handle_type;

#define DEFINE_HANDLE_BODY(handle_type_name, handle_type, content_type) \
	static inline content_type* handle_type_name##Get(handle_type handle) { return (content_type*)handle; }

#define CMP_LESS(asc)    ((asc) ? -1 : 1)
#define CMP_GREATER(asc) ((asc) ? 1 : -1)

/** @brief A number that describes the order of two items (`1` -> greater, `-1` -> lesser, `0` -> equal). */
typedef int_fast8_t Cmp_t;

/** @brief A c-style-string. An array of characters with trailing additional character `NUL`. */
typedef const char* Str_t;

/** @brief The reason for an error as a string. */
typedef Str_t ErrorReasonStr_t;

/** @brief An action that can be taken. */
typedef void (*ActionFn_t)();

/** @brief A predicate testing an item using a closure. */
typedef bool (*PredicateFn_t)(void* pClosure, void* pItem);

/** @brief A comparer that determines the order of two items. */
typedef Cmp_t (*CompareFn_t)(void* left, void* right);

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint8_BitCount(uint8_t value);

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint16_BitCount(uint16_t value);

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint32_BitCount(uint32_t value);

/** @brief Returns the minimum number of bits required to represent x; the result is 0 for x == 0.

	Adapted from the go runtime sources of `math/bits/bits.go`.
*/
size_t uint64_BitCount(uint64_t value);

#endif // __UTIL__
