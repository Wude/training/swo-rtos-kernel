#ifndef __TASK_H__
#define __TASK_H__

#include "array_list.h"
#include "buffer.h"
#include "linked_list.h"
#include "util.h"

#include <stdint.h>

/*****************************************************************************
* Types
******************************************************************************/
/** @brief A caller stack frame (byte size = 8 * 4 = 32).

	When an exception occurs a caller stack frame will be managed by the system.
	It must be prepared for a newly starting task.
*/
typedef struct CallerStackFrame {
	volatile uint32_t R0;   //< General Purpose Register 0, Argument/Result/Scratch Register 1
	volatile uint32_t R1;   //< General Purpose Register 1, Argument/Result/Scratch Register 2
	volatile uint32_t R2;   //< General Purpose Register 2, Argument/Scratch Register 3
	volatile uint32_t R3;   //< General Purpose Register 3, Argument/Scratch Register 4
	volatile uint32_t R12;  //< General Purpose Register 12
	volatile uint32_t LR;   //< Link Register, General Purpose Register 14
	volatile uint32_t PC;   //< Program Counter, Return Address, General Purpose Register 15
	volatile uint32_t xPSR; //< Program Status Register
} volatile CallerStackFrame_t;

/** @brief A callee stack frame (byte size = 9 * 4 = 36).

	Each function that gets called is expected to manage a callee stack frame by itself when needed.
	For that reason it must as well be handled when changing context.
*/
typedef struct CalleeStackFrame {
	volatile uint32_t R4;  //< General Purpose Register 4, Variable Register 1
	volatile uint32_t R5;  //< General Purpose Register 5, Variable Register 2
	volatile uint32_t R6;  //< General Purpose Register 6, Variable Register 3
	volatile uint32_t R7;  //< General Purpose Register 7, Variable Register 4
	volatile uint32_t R8;  //< General Purpose Register 8, Variable Register 5
	volatile uint32_t R9;  //< General Purpose Register 9, Variable Register 6
	volatile uint32_t R10; //< General Purpose Register 10, Variable Register 7
	volatile uint32_t R11; //< General Purpose Register 11, Variable Register 8, Frame Pointer
	volatile uint32_t LR;  //< Link Register, General Purpose Register 14
} volatile CalleeStackFrame_t;

/** @brief A caller stack frame for the registers used by the FPU (byte size = 9 * 8 = 72). */
typedef struct FpuCallerStackFrame {
	union {
		struct {
			volatile uint32_t S0;  //< Single-Precision Floating-Point Register 0
			volatile uint32_t S1;  //< Single-Precision Floating-Point Register 1
			volatile uint32_t S2;  //< Single-Precision Floating-Point Register 2
			volatile uint32_t S3;  //< Single-Precision Floating-Point Register 3
			volatile uint32_t S4;  //< Single-Precision Floating-Point Register 4
			volatile uint32_t S5;  //< Single-Precision Floating-Point Register 5
			volatile uint32_t S6;  //< Single-Precision Floating-Point Register 6
			volatile uint32_t S7;  //< Single-Precision Floating-Point Register 7
			volatile uint32_t S8;  //< Single-Precision Floating-Point Register 8
			volatile uint32_t S9;  //< Single-Precision Floating-Point Register 9
			volatile uint32_t S10; //< Single-Precision Floating-Point Register 10
			volatile uint32_t S11; //< Single-Precision Floating-Point Register 11
			volatile uint32_t S12; //< Single-Precision Floating-Point Register 12
			volatile uint32_t S13; //< Single-Precision Floating-Point Register 13
			volatile uint32_t S14; //< Single-Precision Floating-Point Register 14
			volatile uint32_t S15; //< Single-Precision Floating-Point Register 15
		} S;                       //< Single-Precision Floating-Point Registers 0-15
		struct {
			volatile uint64_t D0; //< Double-Precision Floating-Point Register 0
			volatile uint64_t D1; //< Double-Precision Floating-Point Register 1
			volatile uint64_t D2; //< Double-Precision Floating-Point Register 2
			volatile uint64_t D3; //< Double-Precision Floating-Point Register 3
			volatile uint64_t D4; //< Double-Precision Floating-Point Register 4
			volatile uint64_t D5; //< Double-Precision Floating-Point Register 5
			volatile uint64_t D6; //< Double-Precision Floating-Point Register 6
			volatile uint64_t D7; //< Double-Precision Floating-Point Register 7
		} D;                      //< Double-Precision Floating-Point Registers 0-7
	} R;                          //< Floating-Point Registers S0-S15/D0-D7
	volatile uint32_t FPSCR;      //< Floating-Point Status and Control Register
	volatile uint32_t alignment;  //< Alignment to 64-bit, needed when using the FPU
} volatile FpuCallerStackFrame_t;

/** @brief A callee stack frame for the registers used by the FPU (byte size = 8 * 8 = 64). */
typedef struct FpuCalleeStackFrame {
	union {
		struct {
			volatile uint32_t S16; //< Single-Precision Floating-Point Register 16
			volatile uint32_t S17; //< Single-Precision Floating-Point Register 17
			volatile uint32_t S18; //< Single-Precision Floating-Point Register 18
			volatile uint32_t S19; //< Single-Precision Floating-Point Register 19
			volatile uint32_t S20; //< Single-Precision Floating-Point Register 20
			volatile uint32_t S21; //< Single-Precision Floating-Point Register 21
			volatile uint32_t S22; //< Single-Precision Floating-Point Register 22
			volatile uint32_t S23; //< Single-Precision Floating-Point Register 23
			volatile uint32_t S24; //< Single-Precision Floating-Point Register 24
			volatile uint32_t S25; //< Single-Precision Floating-Point Register 25
			volatile uint32_t S26; //< Single-Precision Floating-Point Register 26
			volatile uint32_t S27; //< Single-Precision Floating-Point Register 27
			volatile uint32_t S28; //< Single-Precision Floating-Point Register 28
			volatile uint32_t S29; //< Single-Precision Floating-Point Register 29
			volatile uint32_t S30; //< Single-Precision Floating-Point Register 30
			volatile uint32_t S31; //< Single-Precision Floating-Point Register 31
		} S;                       //< Single-Precision Floating-Point Registers 16-31
		struct {
			volatile uint64_t D8;  //< Double-Precision Floating-Point Register 8
			volatile uint64_t D9;  //< Double-Precision Floating-Point Register 9
			volatile uint64_t D10; //< Double-Precision Floating-Point Register 10
			volatile uint64_t D11; //< Double-Precision Floating-Point Register 11
			volatile uint64_t D12; //< Double-Precision Floating-Point Register 12
			volatile uint64_t D13; //< Double-Precision Floating-Point Register 13
			volatile uint64_t D14; //< Double-Precision Floating-Point Register 14
			volatile uint64_t D15; //< Double-Precision Floating-Point Register 15
		} D;                       //< Double-Precision Floating-Point Registers 8-15
	} R;                           //< Floating-Point Registers S16-S31/D8-D15
} volatile FpuCalleeStackFrame_t;

/** @brief The type of tasks. */
typedef enum TaskType {
	TaskType_Unknown   = 0, //< An unknown task.
	TaskType_Periodic  = 1, //< A periodic task.
	TaskType_Sporadic  = 2, //< A sporadic task.
	TaskType_Aperiodic = 3, //< A aperiodic task.
} TaskType_t;

/** @brief A task base. */
typedef struct Task {
	TaskType_t taskType;      //< The type of task.
	Str_t      sName;         //< The name of the task.
	void*      pControlBlock; //< The task control block used for execution.
} Task_t;

/** @brief A periodic task.

	A task to be executed periodically with the given period and duration.
*/
typedef struct PeriodicTask PeriodicTask_t;

/** @brief A sporadic task.

	A task to be executed explicitly on demand with a given deadline.
*/
typedef struct SporadicTask SporadicTask_t;

/** @brief A aperiodic task.

	A task to be executed explicitly on demand with a given priority.
*/
typedef struct AperiodicTask AperiodicTask_t;

/** @brief A periodic task.

	A task to be executed periodically with the given period and duration.
*/
struct PeriodicTask {
	Task_t   base;           //< The task base common for task types.
	uint32_t periodExponent; //< The exponent to measure the required period.
	uint32_t durationMs;     //< The scheduled execution duration per period for the task.
};

/** @brief A sporadic task.

	A task to be executed explicitly on demand with a given deadline.
*/
struct SporadicTask {
	Task_t           base;     //< The task base common for task types.
	uint32_t         deadline; //< The priority of the task. A low number means high priority.
	LinkedListNode_t link;     //< A link to sporadic tasks with adjacent deadlines.
};

/** @brief A aperiodic task.

	A task to be executed explicitly on demand with a given priority.
*/
struct AperiodicTask {
	Task_t           base;     //< The task base common for task types.
	uint32_t         priority; //< The priority of the task. A low number means high priority.
	LinkedListNode_t link;     //< A link to aperiodic tasks with adjacent priorities.
};

/*****************************************************************************
* Definitions
******************************************************************************/

// Program Status Register: reset value (set thumb 1).
#define XPSR_RESET_VALUE 0x01000000U

// Special Exception Handler flags stored in the Link Register.
// MSP: use Main Stack Pointer
// PSP: use Process Stack Pointer
// FPU: use Floating Point Unit
#define RETURN_TO_HANDLER_MODE        0xFFFFFFF1U
#define RETURN_TO_THREAD_MODE_MSP     0xFFFFFFF9U
#define RETURN_TO_THREAD_MODE_PSP     0xFFFFFFFDU
#define RETURN_TO_HANDLER_MODE_FPU    0xFFFFFFE1U
#define RETURN_TO_THREAD_MODE_MSP_FPU 0xFFFFFFE9U
#define RETURN_TO_THREAD_MODE_PSP_FPU 0xFFFFFFEDU

// Control Register flags.
#define CONTROL_THREAD_MODE_PSP 0x00000003U

#define PUSH_FRAME_FD(sp, frame_type) (frame_type*)((sp) -= (sizeof(frame_type) / sizeof(uint32_t)))

/** @brief Get the FPU caller stack frame if it was used. */
static inline FpuCallerStackFrame_t* GetFpuCallerStackFrame(CallerStackFrame_t* pCallerStackFrame) {
	// Inverted FPCA bit inside EXC_RETURN indicates FPU usage.
	if ((pCallerStackFrame->LR & 0x10) == 0) {
		return (FpuCallerStackFrame_t*)((size_t)pCallerStackFrame + sizeof(FpuCallerStackFrame_t));
	}
	return NULL;
}

void PeriodicTask_Init(PeriodicTask_t* pPeriodicTask, Str_t sName, uint8_t periodExponent, uint32_t durationMs);
void SporadicTask_Init(SporadicTask_t* pSporadicTask, Str_t sName);
void AperiodicTask_Init(AperiodicTask_t* pAperiodicTask, Str_t sName, uint8_t priority);

Cmp_t SporadicTask_Compare(SporadicTask_t* pLeft, SporadicTask_t* pRight);
Cmp_t AperiodicTask_Compare(AperiodicTask_t* pLeft, AperiodicTask_t* pRight);

DEFINE_ARRAY_LIST_SPEC(PeriodicTaskList, PeriodicTaskList_t, PeriodicTask_t, 32)
DEFINE_ARRAY_LIST_SPEC(SporadicTaskList, SporadicTaskList_t, SporadicTask_t, 32)
DEFINE_ARRAY_LIST_SPEC(AperiodicTaskList, AperiodicTaskList_t, AperiodicTask_t, 32)

DEFINE_LINKED_LIST(SporadicTaskChain, SporadicTaskChain_t, SporadicTask_t, link)
DEFINE_LINKED_LIST(AperiodicTaskChain, AperiodicTaskChain_t, AperiodicTask_t, link)

#endif // __TASK_H__
