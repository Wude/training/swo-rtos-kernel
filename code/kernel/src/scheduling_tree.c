#include "scheduling_tree.h"

#include "array_list.h"

#include <stdint.h>
#include <stdio.h> //< Used for simple debugging with `printf`.

/*****************************************************************************
* Private definitions.
******************************************************************************/

#define ERROR_NO_FREE_SLOT "Unable to create a new scheduling slot"

DEFINE_ARRAY_LIST_BODY(
	SchedulingTreeSlotList, SchedulingTreeSlotList_t, SchedulingTreeSlot_t, SchedulingTreeSlotList_Size())

/** @brief Find `argmin_{pNode}(pNode->branchDurationMs)` for nodes with given `periodExponent`. */
static SchedulingTreeNode_t* FindMinBranchDurationNode(SchedulingTreeNode_t* aNodes, uint32_t periodExponent) {
	SchedulingTreeNode_t* pNode               = NULL;
	uint32_t              minBranchDurationMs = UINT32_MAX;
	uint32_t              nodeIndexStart      = (1 << periodExponent) - 1;
	uint32_t              nodeIndexEnd        = (1 << (periodExponent + 1)) - 1;

	// printf("-- nodeIndexStart = %d; nodeIndexEnd = %d\n", nodeIndexStart, nodeIndexEnd);
	for (uint32_t i = nodeIndexStart; i < nodeIndexEnd; ++i) {
		if (minBranchDurationMs > aNodes[i].branchDurationMs) {
			// printf("-- i = %d\n", i);
			pNode               = &aNodes[i];
			minBranchDurationMs = pNode->branchDurationMs;
		}
	}
	return pNode;
}

/*****************************************************************************
* Definitions.
******************************************************************************/

/** @brief Initialize a scheduling tree.
*
* @param [in] pTree The scheduling tree to initialize.
* @param [in] periodMs The period duration per execution branch.
* @param [in] pPeriodicTasks The periodic tasks to use.
* @return Could the scheduling tree be initialized?
*/
ErrorReasonStr_t SchedulingTree_Init(SchedulingTree_t* pTree, uint32_t periodMs, PeriodicTaskList_t* pPeriodicTasks) {
	SchedulingTreeSlotList_Init(&pTree->slots, 0);

	uint32_t minPeriodExponent = pPeriodicTasks->count == 0 ? 0 : UINT32_MAX;
	uint32_t maxPeriodExponent = 0;

	// Get min/max values.
	for (int i = 0; i < pPeriodicTasks->count; ++i) {
		minPeriodExponent = MIN(minPeriodExponent, pPeriodicTasks->aData[i].periodExponent);
		maxPeriodExponent = MAX(maxPeriodExponent, pPeriodicTasks->aData[i].periodExponent);
	}

	// Resetting the period scale if necessary.
	if (minPeriodExponent > 0) {
		for (int i = 0; i < pPeriodicTasks->count; ++i) {
			pPeriodicTasks->aData[i].periodExponent -= minPeriodExponent;
		}
		periodMs *= 1 << minPeriodExponent;
		maxPeriodExponent -= minPeriodExponent;
		minPeriodExponent = 0;
	}

	pTree->periodMs          = periodMs;
	pTree->maxPeriodExponent = maxPeriodExponent;
	pTree->branchCount       = 1 << maxPeriodExponent;

	// Initialize node structure. Nodes of the same depth are packed together.
	ArrayListSize_t lastNonLeafCount = pTree->branchCount - 1;
	pTree->nodes.count               = pTree->branchCount + lastNonLeafCount;
	pTree->pRoot                     = &pTree->nodes.aData[0];
	for (ArrayListSize_t i = 0; i < pTree->nodes.count; ++i) {
		if (i < lastNonLeafCount) {
			pTree->nodes.aData[i].apChilds[0] = &pTree->nodes.aData[2 * i + 1];
			pTree->nodes.aData[i].apChilds[1] = &pTree->nodes.aData[2 * i + 2];
		} else {
			pTree->nodes.aData[i].apChilds[0] = NULL;
			pTree->nodes.aData[i].apChilds[1] = NULL;
		}
		pTree->nodes.aData[i].pSlot            = NULL;
		pTree->nodes.aData[i].branchDurationMs = 0;
	}

	SchedulingTreeSlot_t* pSlot;
	SchedulingTreeSlot_t* pNextSlot;
	SchedulingTreeNode_t* pNode;
	SchedulingTreeNode_t* pNextNode;

	// Allot the perodic tasks by their period.
	for (uint32_t periodExponent = 0; periodExponent <= maxPeriodExponent; ++periodExponent) {
		for (int i = 0; i < pPeriodicTasks->count; ++i) {
			if (pPeriodicTasks->aData[i].periodExponent == periodExponent) {
				pNode = FindMinBranchDurationNode(pTree->nodes.aData, periodExponent);
				for (pSlot = NULL, pNextSlot = pNode->pSlot; pNextSlot != NULL;
					 pSlot = pNextSlot, pNextSlot = pNextSlot->pNext) { }

				pNextSlot = SchedulingTreeSlotList_AppendItemEmpty(&pTree->slots);
				if (pNextSlot == NULL) { return ERROR_NO_FREE_SLOT; }
				pNextSlot->pNext          = NULL;
				pNextSlot->pPeriodicTask  = &pPeriodicTasks->aData[i];
				pNextSlot->freeDurationMs = 0;

				if (pSlot != NULL) {
					pSlot->pNext = pNextSlot;
				} else {
					pNode->pSlot = pNextSlot; //< Special case - no slots yet.
				}
				pNode->branchDurationMs += pNextSlot->pPeriodicTask->durationMs;
				if (pNode->apChilds[0] != NULL) { pNode->apChilds[0]->branchDurationMs = pNode->branchDurationMs; }
				if (pNode->apChilds[1] != NULL) { pNode->apChilds[1]->branchDurationMs = pNode->branchDurationMs; }
			}
		}
	}

	// Filling the free slots.
	for (uint32_t branchIndex = 0; branchIndex < pTree->branchCount; ++branchIndex) {
		// Get the last slots in this branch.
		uint32_t periodIndex;
		pNode = NULL;
		pSlot = NULL;
		// printf("branchIndex = %d, (branchIndex >> periodIndex) & 1 = ", branchIndex);
		for (pNextNode = pTree->pRoot, periodIndex = maxPeriodExponent; pNextNode != NULL;
			 pNode = pNextNode, pNextNode = pNextNode->apChilds[(branchIndex >> periodIndex) & 1]) {
			// printf("%d", (branchIndex >> (periodIndex + 1)) & 1);
			--periodIndex;
		}
		// printf("; pSlot = %p\n", pSlot);
		if (pNode == NULL) {
			pNode = pTree->pRoot;
			pSlot = pNode->pSlot;
		} else {
			for (pSlot = NULL, pNextSlot = pNode->pSlot; pNextSlot != NULL && pNextSlot->pPeriodicTask != NULL;
				 pSlot = pNextSlot, pNextSlot = pNextSlot->pNext) { }
		}

		// Fill the duration by creating free duration slots.
		if (pNode->branchDurationMs < periodMs) {
			pNextSlot = SchedulingTreeSlotList_AppendItemEmpty(&pTree->slots);
			if (pNextSlot == NULL) { return ERROR_NO_FREE_SLOT; }
			pNextSlot->pNext          = NULL;
			pNextSlot->pPeriodicTask  = NULL;
			pNextSlot->freeDurationMs = periodMs - pNode->branchDurationMs;

			if (pSlot != NULL) {
				pSlot->pNext = pNextSlot;
			} else {
				pNode->pSlot = pNextSlot; //< Special case - no slots yet.
			}
			pNode->branchDurationMs += pNextSlot->freeDurationMs;
			if (pNode->apChilds[0] != NULL) { pNode->apChilds[0]->branchDurationMs = pNode->branchDurationMs; }
			if (pNode->apChilds[1] != NULL) { pNode->apChilds[1]->branchDurationMs = pNode->branchDurationMs; }
		}
	}
	return NULL;
}

SchedulingTreeSlot_t* SchedulingTreeIterator_Init(SchedulingTreeIterator_t* pIterator, SchedulingTree_t* pTree) {
	pIterator->pTree       = pTree;
	pIterator->cycleCount  = 0;
	pIterator->branchIndex = 0;
	pIterator->periodIndex = pIterator->pTree->maxPeriodExponent - 1;
	pIterator->pNode       = pIterator->pTree->pRoot;
	pIterator->pSlot       = pIterator->pNode->pSlot;
	return pIterator->pSlot;
}

SchedulingTreeSlot_t* SchedulingTreeIterator_Next(SchedulingTreeIterator_t* pIterator) {
	// if (pIterator->pSlot->pPeriodicTask != NULL) {
	// 	printf(
	// 		"branchIndex = %d, periodIndex = %d, pPeriodicTask = %s\n", pIterator->branchIndex, pIterator->periodIndex,
	// 		pIterator->pSlot->pPeriodicTask->base.sName);
	// } else {
	// 	printf(
	// 		"branchIndex = %d, periodIndex = %d, freeDurationMs = %d\n", pIterator->branchIndex, pIterator->periodIndex,
	// 		pIterator->pSlot->freeDurationMs);
	// }
	pIterator->pSlot = pIterator->pSlot->pNext;
	while (pIterator->pSlot == NULL) {
		pIterator->pNode = pIterator->pNode->apChilds[(pIterator->branchIndex >> pIterator->periodIndex) & 1];
		// printf(
		// 	"branchIndex = %d, periodIndex = %d, (pIterator->branchIndex >> pIterator->periodIndex) & 1 = %d\n",
		// 	pIterator->branchIndex, pIterator->periodIndex, (pIterator->branchIndex >> pIterator->periodIndex) & 1);
		--pIterator->periodIndex;
		if (pIterator->pNode != NULL) {
			pIterator->pSlot = pIterator->pNode->pSlot;
		} else {
			if (++pIterator->branchIndex >= pIterator->pTree->branchCount) {
				pIterator->branchIndex = 0;
				++pIterator->cycleCount;
				// printf("++cycleCount\n");
			}
			pIterator->periodIndex = pIterator->pTree->maxPeriodExponent - 1;
			pIterator->pNode       = pIterator->pTree->pRoot;
			pIterator->pSlot       = pIterator->pNode->pSlot;
			// printf("----------\n");
		}
	}
	return pIterator->pSlot;
}
