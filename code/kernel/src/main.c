#include "itm.h"
#include "kernel.h"
#include "system.h"
#include "uart.h"
#include "util.h"

#define TIME_FACTOR 100

#define STACK_SIZE 1024
uint64_t aStackA[STACK_SIZE];
uint64_t aStackB[STACK_SIZE];
uint64_t aStackC[STACK_SIZE];
uint64_t aStackD[STACK_SIZE];
uint64_t aStackE[STACK_SIZE];
uint64_t aStackF[STACK_SIZE];

// #define SEND_PREFIX(name) ITM##name
#define SEND_PREFIX(name) UART##name

#define SendChar   SEND_PREFIX(_SendChar)
#define SendString SEND_PREFIX(_SendString)
#define SendUint32 SEND_PREFIX(_SendUint32)

void TaskA() {
	// Simply report the task as running.
	SendString("A: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

void TaskB() {
	// Simply report the task as running.
	SendString("B: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

void TaskC() {
	// Simply report the task as running.
	SendString("C: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

void TaskD() {
	// Simply report the task as running.
	SendString("D: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

void TaskE() {
	// Simply report the task as running.
	SendString("E: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

void TaskF() {
	// Simply report the task as running.
	SendString("F: ");
	SendUint32(Kernel_GetCycleCount());
	SendChar('/');
	SendUint32(Kernel_GetBranchIndex());
	SendChar('/');
	SendUint32(Kernel_GetElapsedSystemTimeMs() / TIME_FACTOR);
	SendString("\r\n");
}

int main(void) {
	System_Init();
	UART_Init();
	Kernel_Init();

	UART_SendString("\r\nCompiled: " __DATE__ ", " __TIME__ "\r\n");
	UART_SendString("RTOS-Mini-Kernel-Test...\r\n----------\r\n");

	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackA, TaskA, "A", 1, 4 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"A\".");
	}
	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackB, TaskB, "B", 0, 3 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"B\".");
	}
	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackC, TaskC, "C", 2, 2 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"C\".");
	}
	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackD, TaskD, "D", 1, 1 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"D\".");
	}
	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackE, TaskE, "E", 0, 1 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"E\".");
	}
	if (!Kernel_CreatePeriodicTask(STACK_SIZE, aStackF, TaskF, "F", 2, 5 * TIME_FACTOR)) {
		UART_SendString("Unable to create task \"F\".");
	}

	ErrorReasonStr_t sReason = Kernel_Start(10 * TIME_FACTOR);
	if (sReason == NULL) {
		UART_SendString("Kernel start returned without error although it shouldn't have returned at all");
	} else {
		UART_SendString(sReason);
	}
}
