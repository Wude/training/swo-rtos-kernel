#ifndef __BUFFER__
#define __BUFFER__

#include "util.h"

/* The type used for indexing the data in the buffers. */
typedef int32_t BufferSize_t;
/* "BufferSize_fm" holds the format used for "printf". */
#define BufferSize_fm PRIi32
/* "BufferSize_size" must match `sizeof(BufferSize_t)` */
#define BufferSize_size 4

/*******************************************************************************
* generic buffer - type dependent parts
********************************************************************************
* The pragma pack is neccessary to ensure the proper alignment
* for item types larger than "BufferSize_size".
*******************************************************************************/

#define DEFINE_BUFFER_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BufferSize_size) \
	/** @brief An buffer of items with type "list_type" and size "list_size" */ \
	typedef struct list_type_name { \
		volatile BufferSize_t count; \
		BufferSize_t          writeIndex; \
		BufferSize_t          readIndex; \
		item_type             aData[list_size]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_BUFFER_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	/** @brief Get the buffer size. */ \
	static inline BufferSize_t list_type_name##_Size() { return list_size; } \
	/** @brief Get the number of items in the buffer. */ \
	static inline BufferSize_t list_type_name##_GetCount(list_type* pBuffer) { return pBuffer->count; } \
	/** @brief Initialize the buffer. */ \
	static inline void list_type_name##_Init(list_type* pBuffer) { \
		pBuffer->count      = 0; \
		pBuffer->writeIndex = 0; \
		pBuffer->readIndex  = 0; \
	} \
	/** @brief Remove the front item from the buffer and check for success. */ \
	bool list_type_name##_TryRead(list_type* pBuffer, item_type* pItem); \
	/** @brief Remove the front item from the buffer. */ \
	item_type list_type_name##_Read(list_type* pBuffer); \
	/** @brief Append a single item to the buffer and check for success. */ \
	bool list_type_name##_TryWrite(list_type* pBuffer, item_type item); \
	/** @brief Append a single item to the buffer. */ \
	void list_type_name##_Write(list_type* pBuffer, item_type item);

#define DEFINE_BUFFER_BODY(list_type_name, list_type, item_type, list_size) \
\
	bool list_type_name##_TryRead(list_type* pBuffer, item_type* pItem) { \
		if (pBuffer->count <= 0) { return false; } \
		*pItem = pBuffer->aData[pBuffer->readIndex++]; \
		if (pBuffer->readIndex >= list_size) { pBuffer->readIndex = 0; } \
		--pBuffer->count; \
		return true; \
	} \
\
	item_type list_type_name##_Read(list_type* pBuffer) { \
		if (pBuffer->count <= 0) { return (item_type)0; } \
		item_type item = pBuffer->aData[pBuffer->readIndex++]; \
		if (pBuffer->readIndex >= list_size) { pBuffer->readIndex = 0; } \
		--pBuffer->count; \
		return item; \
	} \
\
	bool list_type_name##_TryWrite(list_type* pBuffer, item_type item) { \
		if (pBuffer->count >= list_size) { return false; } \
		pBuffer->aData[pBuffer->writeIndex++] = item; \
		if (pBuffer->writeIndex >= list_size) { pBuffer->writeIndex = 0; } \
		++pBuffer->count; \
		return true; \
	} \
\
	void list_type_name##_Write(list_type* pBuffer, item_type item) { \
		if (pBuffer->count >= list_size) { return; } \
		pBuffer->aData[pBuffer->writeIndex++] = item; \
		if (pBuffer->writeIndex >= list_size) { pBuffer->writeIndex = 0; } \
		++pBuffer->count; \
	}

#define DEFINE_BUFFER_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_BUFFER_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BUFFER_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_BUFFER(list_type_name, list_type, item_type, list_size) \
	DEFINE_BUFFER_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BUFFER_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BUFFER_BODY(list_type_name, list_type, item_type, list_size)

/*******************************************************************************
* generic buffer - type independent parts
*******************************************************************************/

/* No type independent parts. */

#endif // __BUFFER__
