#ifndef __UART__
#define __UART__

#include "buffer.h"
#include "util.h"

/** @brief Initialize USART2. */
void UART_Init();

/** @brief Get the number of characters to receive over USART2.

	@return The number of characters to receive.
*/
BufferSize_t UART_GetCount();

/** @brief Receive a character over USART2.

	@param [in] c The character to receive.
*/
char UART_ReceiveChar();

/** @brief Send a character over USART2.

	@param [in] c The character to send.
*/
void UART_SendChar(char c);

/** @brief Send data over USART2.

	@param [in] aData The data to send.
	@param [in] count The number of characters to send.
*/
void UART_SendData(const char* aData, size_t len);

/** @brief Send a string over USART2.

	@param [in] sText The string to send.
*/
void UART_SendString(Str_t sText);

/** @brief Send a 32-bit unsigned integer over USART2.

	@param [in] value The 32-bit unsigned integer to send.
*/
void UART_SendUint32(uint32_t value);

#endif // __UART__
