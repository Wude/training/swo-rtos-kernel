#ifndef __LIST_H__
#define __LIST_H__

#include "array_list.h"
#include "util.h"

/*******************************************************************************
* generic linked list - type dependent parts
********************************************************************************
* use the generic part for better type safety.
*******************************************************************************/

/* The type used for indexing the data in the linked lists. */
typedef int32_t LinkedListSize_t;
/* "LinkedListSize_fm" holds the format used for "printf". */
#define LinkedListSize_fm PRIi32
/* "LinkedListSize_size" must match `sizeof(LinkedListSize_t)` */
#define LinkedListSize_size 4

#define DEFINE_LINKED_LIST_TYPE_ONLY(list_type_name, list_type, item_type) \
	/** @brief An linked list of items with type "list_type". */ \
	typedef struct list_type_name { \
		LinkedListSize_t  count; \
		LinkedListNode_t* pFirst; \
		LinkedListNode_t* pLast; \
	} list_type;

#define DEFINE_LINKED_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, node_member) \
	static inline item_type* list_type_name##_GetItem(LinkedListNode_t* pNode) { \
		return (item_type*)((intptr_t)pNode - offsetof(item_type, node_member)); \
	} \
	static inline item_type* list_type_name##_GetItemNullable(LinkedListNode_t* pNode) { \
		return pNode == NULL ? NULL : list_type_name##_GetItem(pNode); \
	} \
	static inline void list_type_name##_Init(list_type* pList) { LinkedList_Init((LinkedList_t*)pList); } \
	static inline void list_type_name##_Prepend(list_type* pList, item_type* pNewNode) { \
		LinkedList_Prepend((LinkedList_t*)pList, (LinkedListNode_t*)&pNewNode->node_member); \
	} \
	static inline void list_type_name##_Append(list_type* pList, item_type* pNewNode) { \
		LinkedList_Append((LinkedList_t*)pList, (LinkedListNode_t*)&pNewNode->node_member); \
	} \
	static inline void list_type_name##_InsertBefore(list_type* pList, item_type* pOldNode, item_type* pNewNode) { \
		LinkedList_InsertBefore( \
			(LinkedList_t*)pList, (LinkedListNode_t*)&pOldNode->node_member, \
			(LinkedListNode_t*)&pNewNode->node_member); \
	} \
	static inline void list_type_name##_InsertAfter(list_type* pList, item_type* pOldNode, item_type* pNewNode) { \
		LinkedList_InsertAfter( \
			(LinkedList_t*)pList, (LinkedListNode_t*)&pOldNode->node_member, \
			(LinkedListNode_t*)&pNewNode->node_member); \
	} \
	static inline void list_type_name##_InsertFromFront( \
		list_type* pList, item_type* pNewNode, CompareFn_t fCompare, bool asc) { \
		LinkedList_InsertFromFront( \
			(LinkedList_t*)pList, (LinkedListNode_t*)&pNewNode->node_member, fCompare, asc, \
			offsetof(item_type, node_member)); \
	} \
	static inline void list_type_name##_InsertFromBehind( \
		list_type* pList, item_type* pNewNode, CompareFn_t fCompare, bool asc) { \
		LinkedList_InsertFromBehind( \
			(LinkedList_t*)pList, (LinkedListNode_t*)&pNewNode->node_member, fCompare, asc, \
			offsetof(item_type, node_member)); \
	} \
	static inline void list_type_name##_Remove(list_type* pList, item_type* pNode) { \
		LinkedList_Remove((LinkedList_t*)pList, (LinkedListNode_t*)&pNode->node_member); \
	} \
	static inline void list_type_name##_Sort(list_type* pList, CompareFn_t fCompare, bool asc) { \
		LinkedList_Sort((LinkedList_t*)pList, fCompare, asc, offsetof(item_type, node_member)); \
	}

#define DEFINE_LINKED_LIST_BODY(list_type_name, list_type, item_type, node_member)

#define DEFINE_LINKED_LIST_SPEC(list_type_name, list_type, item_type, node_member) \
	DEFINE_LINKED_LIST_TYPE_ONLY(list_type_name, list_type, item_type) \
	DEFINE_LINKED_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, node_member)

#define DEFINE_LINKED_LIST(list_type_name, list_type, item_type, node_member) \
	DEFINE_LINKED_LIST_TYPE_ONLY(list_type_name, list_type, item_type) \
	DEFINE_LINKED_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, node_member) \
	DEFINE_LINKED_LIST_BODY(list_type_name, list_type, item_type, node_member)

/*******************************************************************************
* generic linked list - type independent parts
*******************************************************************************/

/** @brief A node in a linked list. */
typedef struct LinkedListNode LinkedListNode_t;

/** @brief A node in a linked list. */
struct LinkedListNode {
	LinkedListNode_t* pPrev;
	LinkedListNode_t* pNext;
};

/** @brief A base for linked lists. */
typedef struct LinkedList {
	LinkedListSize_t  count;
	LinkedListNode_t* pFirst;
	LinkedListNode_t* pLast;
} LinkedList_t;

void LinkedList_Init(LinkedList_t* pList);
void LinkedList_Prepend(LinkedList_t* pList, LinkedListNode_t* pNewNode);
void LinkedList_Append(LinkedList_t* pList, LinkedListNode_t* pNewNode);
void LinkedList_InsertBefore(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode);
void LinkedList_InsertAfter(LinkedList_t* pList, LinkedListNode_t* pOldNode, LinkedListNode_t* pNewNode);
void LinkedList_InsertFromFront(
	LinkedList_t* pList, LinkedListNode_t* pNewNode, CompareFn_t fCompare, bool asc, size_t offset);
void LinkedList_InsertFromBehind(
	LinkedList_t* pList, LinkedListNode_t* pNewNode, CompareFn_t fCompare, bool asc, size_t offset);
void LinkedList_Remove(LinkedList_t* pList, LinkedListNode_t* pNode);
void LinkedList_Sort(LinkedList_t* pList, CompareFn_t fCompare, bool asc, size_t offset);

#endif // __LIST_H__
