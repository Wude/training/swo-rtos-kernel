#include "ports.h"

#include "stm32f4xx_gpio.h"

void Ports_Init() { }

bool GPIO_GetBool(GPIO_TypeDef* pPort, short pin) { return pPort->IDR >> pin & 0x01; }

void GPIO_SetBool(GPIO_TypeDef* pPort, short pin) { pPort->BSRRL = 1 << pin; }

void GPIO_ResetBool(GPIO_TypeDef* pPort, short pin) { pPort->BSRRH = 1 << pin; }

void GPIO_ToggleBool(GPIO_TypeDef* pPort, short pin) {
	uint32_t nBitMask = 1 << pin;
	nBitMask          = (nBitMask << 16) | (~pPort->ODR & nBitMask);
	pPort->BSRR       = nBitMask;
}
