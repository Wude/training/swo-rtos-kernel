#ifndef __ITM_H__
#define __ITM_H__

#include "util.h"

/*****************************************************************************
* Definitions
******************************************************************************/

/** @brief Send data to the ITM debug port 0.

	@param [in] aData The data to send.
	@param [in] count The number of characters to send.
*/
void ITM_SendData(const char* aData, size_t len);

/** @brief Send a string to the ITM debug port 0.

	@param [in] sText The string to send.
*/
void ITM_SendString(Str_t sText);

/** @brief Send a 32-bit unsigned integer to the ITM debug port 0.

	@param [in] value The 32-bit unsigned integer to send.
*/
void ITM_SendUint32(uint32_t value);

#endif // __ITM_H__
