#ifndef __BIT_LIST__
#define __BIT_LIST__

#include "util.h"

/* The type used for indexing the data in the array lists. */
typedef int32_t BitListSize_t;
/* "BitListSize_fm" holds the format used for "printf". */
#define BitListSize_fm PRIi32
/* "BitListSize_size" must match `sizeof(BitListSize_t)` */
#define BitListSize_size 4

/*******************************************************************************
* generic bit list - type dependent parts
********************************************************************************
* The union item definitions are useful for debugging,
* the generic implementation does not depend on it.
*******************************************************************************/

#define DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t       itemsPerByte; \
		const BitListSize_t size; \
		BitListSize_t       length; \
		union { \
			uint8_t byte; \
			uint8_t item0 : 8; \
		} aData[list_size]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t       itemsPerByte; \
		const BitListSize_t size; \
		BitListSize_t       length; \
		union { \
			uint8_t byte; \
			struct { \
				uint8_t item0 : 4; \
				uint8_t item1 : 4; \
			} items; \
		} aData[list_size / 2]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t       itemsPerByte; \
		const BitListSize_t size; \
		BitListSize_t       length; \
		union { \
			uint8_t byte; \
			struct { \
				uint8_t item0 : 2; \
				uint8_t item1 : 2; \
				uint8_t item2 : 2; \
				uint8_t item3 : 2; \
			} items; \
		} aData[list_size / 4]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	PRAGMA_PACK(BitListSize_size) \
	typedef struct list_type_name { \
		const uint8_t       itemsPerByte; \
		const BitListSize_t size; \
		BitListSize_t       length; \
		union { \
			uint8_t byte; \
			struct { \
				uint8_t item0 : 1; \
				uint8_t item1 : 1; \
				uint8_t item2 : 1; \
				uint8_t item3 : 1; \
				uint8_t item4 : 1; \
				uint8_t item5 : 1; \
				uint8_t item6 : 1; \
				uint8_t item7 : 1; \
			} items; \
		} aData[list_size / 8]; \
	} list_type; \
	PRAGMA_PACK()

#define DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size) \
	/** @brief Initialize the list with an item count. */ \
	void list_type_name##_Init(list_type* pBitList, BitListSize_t len); \
	/** @brief Try to get an item from the list. */ \
	bool list_type_name##_TryGetItem(list_type* pBitList, BitListSize_t pos, item_type* item); \
	/** @brief Get an item from the list. */ \
	item_type list_type_name##_GetItem(list_type* pBitList, BitListSize_t pos); \
	/** @brief Set an item in the list. */ \
	void list_type_name##_SetItem(list_type* pBitList, BitListSize_t pos, item_type item); \
	/** @brief Append a single item to the list. */ \
	bool list_type_name##_AppendItem(list_type* pBitList, item_type item); \
	/** @brief Find the index for the given item. Returns "-1" when not found. */ \
	BitListSize_t list_type_name##_FindIndex(list_type* pBitList, PredicateFn_t fPredicate, void* pData);

#define DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, bits_per_item) \
\
	void list_type_name##_Init(list_type* pBitList, BitListSize_t len) { \
		BitList_Init((BitList_t*)pBitList, 8 / bits_per_item, list_size, len); \
	} \
\
	bool list_type_name##_TryGetItem(list_type* pBitList, BitListSize_t pos, item_type* item) { \
		uint8_t byte; \
		bool    success = BitList_TryGetItem##bits_per_item((BitList_t*)pBitList, pos, &byte); \
		*item           = (item_type)byte; \
		return success; \
	} \
\
	item_type list_type_name##_GetItem(list_type* pBitList, BitListSize_t pos) { \
		return (item_type)BitList_GetItem##bits_per_item((BitList_t*)pBitList, pos); \
	} \
\
	void list_type_name##_SetItem(list_type* pBitList, BitListSize_t pos, item_type item) { \
		BitList_SetItem##bits_per_item((BitList_t*)pBitList, pos, (uint8_t)item); \
	} \
\
	bool list_type_name##_AppendItem(list_type* pBitList, item_type item) { \
		return BitList_AppendItem##bits_per_item((BitList_t*)pBitList, (uint8_t)item); \
	} \
\
	BitListSize_t list_type_name##_FindIndex(list_type* pBitList, PredicateFn_t fPredicate, void* pData) { \
		return BitList_FindIndex##bits_per_item((BitList_t*)pBitList, predicate, data); \
	}

/******************************************************************************/
#define DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_SPEC_WITHOUT_TYPE_INTERNAL(list_type_name, list_type, item_type, list_size)

/******************************************************************************/
#define DEFINE_8BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 8)

#define DEFINE_4BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 4)

#define DEFINE_2BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 2)

#define DEFINE_1BIT_LIST_BODY(list_type_name, list_type, item_type, list_size) \
	DEFINE_BIT_LIST_BODY_INTERNAL(list_type_name, list_type, item_type, list_size, 1)

/******************************************************************************/
#define DEFINE_8BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST_SPEC(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size)

/******************************************************************************/
#define DEFINE_8BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_8BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_4BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_4BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_2BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_2BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

#define DEFINE_1BIT_LIST(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_TYPE_ONLY(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_SPEC_WITHOUT_TYPE(list_type_name, list_type, item_type, list_size) \
	DEFINE_1BIT_LIST_BODY(list_type_name, list_type, item_type, list_size)

/*******************************************************************************
* generic bit list - type independent parts
*******************************************************************************/

DEFINE_8BIT_LIST_TYPE_ONLY(BitList, BitList_t, uint8_t, EMTPY);

void          BitList_Init(BitList_t* pBitList, uint8_t itemsPerByte, BitListSize_t size, BitListSize_t len);
bool          BitList_TryGetItem8(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem);
bool          BitList_TryGetItem4(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem);
bool          BitList_TryGetItem2(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem);
bool          BitList_TryGetItem1(BitList_t* pBitList, BitListSize_t pos, uint8_t* pItem);
uint8_t       BitList_GetItem8(BitList_t* pBitList, BitListSize_t pos);
uint8_t       BitList_GetItem4(BitList_t* pBitList, BitListSize_t pos);
uint8_t       BitList_GetItem2(BitList_t* pBitList, BitListSize_t pos);
uint8_t       BitList_GetItem1(BitList_t* pBitList, BitListSize_t pos);
void          BitList_SetItem8(BitList_t* pBitList, BitListSize_t pos, uint8_t item);
void          BitList_SetItem4(BitList_t* pBitList, BitListSize_t pos, uint8_t item);
void          BitList_SetItem2(BitList_t* pBitList, BitListSize_t pos, uint8_t item);
void          BitList_SetItem1(BitList_t* pBitList, BitListSize_t pos, uint8_t item);
bool          BitList_AppendItem8(BitList_t* pBitList, uint8_t item);
bool          BitList_AppendItem4(BitList_t* pBitList, uint8_t item);
bool          BitList_AppendItem2(BitList_t* pBitList, uint8_t item);
bool          BitList_AppendItem1(BitList_t* pBitList, uint8_t item);
BitListSize_t BitList_FindIndex8(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure);
BitListSize_t BitList_FindIndex4(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure);
BitListSize_t BitList_FindIndex2(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure);
BitListSize_t BitList_FindIndex1(BitList_t* pBitList, PredicateFn_t fPredicate, void* pClosure);

#endif // __BIT_LIST__
