#include "gtest/gtest.h"

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

extern "C" {
#include "task.h"

#define STACK_SIZE ((sizeof(CallerStackFrame_t) + sizeof(CalleeStackFrame_t)) / sizeof(uint32_t))
uint32_t aStack[STACK_SIZE];
}

TEST(StackFrameTest, PushAccess) {
	const size_t frameSize = STACK_SIZE;
	uint32_t*    pStack    = aStack + frameSize;

	CallerStackFrame_t* pCallerStackFrame = PUSH_FRAME_FD(pStack, CallerStackFrame_t);
	CalleeStackFrame_t* pCalleeStackFrame = PUSH_FRAME_FD(pStack, CalleeStackFrame_t);

	pCallerStackFrame->xPSR = XPSR_RESET_VALUE;
	pCallerStackFrame->PC   = 0x2000;
	pCallerStackFrame->LR   = 0x4000;
	pCalleeStackFrame->LR   = RETURN_TO_THREAD_MODE_PSP;

	for (size_t i = 0; i < frameSize; ++i) { printf("aStack[%" PRIuPTR "] = %" PRIx32 "\n", i, aStack[i]); }
}
