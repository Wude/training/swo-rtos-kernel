#include "gtest/gtest.h"

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

extern "C" {
#include "util.h"

static const Str_t asBitStrings[] = {"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
									 "1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111"};

void printCeil(uint8_t value) {
	uint8_t ceiledValue = CEIL_MULTIPLE_OF_TWO(value);
	printf(
		"CEIL_MULTIPLE_OF_TWO(%" PRIu8 " -> %s) = %" PRIu8 " -> %s\n", value, asBitStrings[value & 3], ceiledValue,
		asBitStrings[ceiledValue & 3]);
}
}

TEST(MiscTest, Snprintf) {
	uint32_t value = 1234;
	int      count = 10;
	char     aChars[count];

	// _itoa_s(value, aChars, 10, 10);
	count = snprintf(aChars, count, "%" PRIu32, value);
	for (int i = 0; i < count; ++i) { printf("%c", aChars[i]); }
	printf("\n");
}

TEST(MiscTest, IndDec) {
	uint32_t i = 0;
	printf("         i = %d;\n", i);
	// i = 0;
	printf("i++ = %d; ", i++);
	printf("i = %d;\n", i);
	// i = 0;
	printf("++i = %d; ", ++i);
	printf("i = %d;\n", i);
}

TEST(MiscTest, StackPointer) {
	// uint64_t aaStack[64][4];
	uint64_t aaStack[4][64];
	printf("&aaStack[0][0]                                      = %" PRIuPTR ";\n", (intptr_t)&aaStack[0][0]);
	printf("&aaStack[0][1]                                      = %" PRIuPTR ";\n", (intptr_t)&aaStack[0][1]);
	printf("&aaStack[1][0]                                      = %" PRIuPTR ";\n", (intptr_t)&aaStack[1][0]);
	printf("&aaStack[1][0]          - &aaStack[0][0]            = %" PRIuPTR ";\n", (&aaStack[1][0] - &aaStack[0][0]));
	printf(
		"(intptr_t)&aaStack[1][0] - (intptr_t)&aaStack[0][0] = %" PRIuPTR ";\n",
		((intptr_t)&aaStack[1][0] - (intptr_t)&aaStack[0][0]));
	printf("&aaStack[0][1]          - &aaStack[0][0]            = %" PRIuPTR ";\n", (&aaStack[0][1] - &aaStack[0][0]));
	printf(
		"(intptr_t)&aaStack[0][1] - (intptr_t)&aaStack[0][0] = %" PRIuPTR ";\n",
		((intptr_t)&aaStack[0][1] - (intptr_t)&aaStack[0][0]));

	printf("--\n");

	uint64_t* aStack      = aaStack[0];
	uint64_t* aStackLower = aStack;
	uint64_t* aStackUpper = aStackLower + 64;
	printf("aStack                                              = %" PRIuPTR ";\n", (intptr_t)aStack);
	printf("aStackLower                                         = %" PRIuPTR ";\n", (intptr_t)aStackLower);
	printf("aStackUpper                                         = %" PRIuPTR ";\n", (intptr_t)aStackUpper);
	printf(
		"aStackUpper             - aStackLower               = %" PRIuPTR ";\n", (intptr_t)(aStackUpper - aStackLower));
	printf(
		"(intptr_t)aStackUpper   - (intptr_t)aStackLower     = %" PRIuPTR ";\n",
		((intptr_t)aStackUpper - (intptr_t)aStackLower));
}

TEST(MiscTest, CeilMultipleOfTwo) {
	printCeil(0);
	printCeil(1);
	printCeil(2);
	printCeil(3);
	printCeil(4);
	printCeil(5);
	printCeil(6);
	printCeil(7);
}
