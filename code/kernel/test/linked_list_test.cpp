#include "gtest/gtest.h"

extern "C" {
#include "linked_list.h"

typedef struct TestListItem {
	uint32_t         id;
	uint32_t         priority;
	LinkedListNode_t link;
} TestListItem_t;

DEFINE_LINKED_LIST(TestList, TestList_t, TestListItem_t, link)

Cmp_t CompareTestListItemPriority(TestListItem_t* pLeft, TestListItem_t* pRight) {
	if (pLeft->priority < pRight->priority) { return -1; }
	if (pLeft->priority > pRight->priority) { return 1; }
	return 0;
}

size_t         g_nodeCount = 0;
TestListItem_t g_aNodes[64];
TestList_t     g_list;

#define TEST_LIST_APPEND() TestList_Append(&g_list, &g_aNodes[g_nodeCount++])
#define TEST_LIST_INSERT_FROM_FRONT() \
	TestList_InsertFromFront(&g_list, &g_aNodes[g_nodeCount++], (CompareFn_t)CompareTestListItemPriority, true)
#define TEST_LIST_INSERT_FROM_BEHIND() \
	TestList_InsertFromBehind(&g_list, &g_aNodes[g_nodeCount++], (CompareFn_t)CompareTestListItemPriority, true)
#define TEST_LIST_INSERT() TEST_LIST_INSERT_FROM_FRONT()
// #define TEST_LIST_INSERT() TEST_LIST_INSERT_FROM_BEHIND()
}

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

TEST(LinkedListTest, InsertRemoveSortCount) {
	TestListItem_t* pItem;

	TestList_Init(&g_list);

	g_aNodes[g_nodeCount].id       = 0;
	g_aNodes[g_nodeCount].priority = 17;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 1;
	g_aNodes[g_nodeCount].priority = 5;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 2;
	g_aNodes[g_nodeCount].priority = 416;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 3;
	g_aNodes[g_nodeCount].priority = 64;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 4;
	g_aNodes[g_nodeCount].priority = 83;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 5;
	g_aNodes[g_nodeCount].priority = 9;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 6;
	g_aNodes[g_nodeCount].priority = 38;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 7;
	g_aNodes[g_nodeCount].priority = 17;
	TEST_LIST_INSERT();
	g_aNodes[g_nodeCount].id       = 8;
	g_aNodes[g_nodeCount].priority = 17;
	TEST_LIST_INSERT();

	EXPECT_EQ(g_list.count, 9);

	LinkedListNode_t* pNode;
	printf("----------------------------------------\n");
	for (pNode = g_list.pFirst; pNode != NULL; pNode = pNode->pNext) {
		pItem = TestList_GetItem(pNode);
		printf("id = %d; priority = %d;\n", pItem->id, pItem->priority);
	}

	EXPECT_EQ(TestList_GetItem(pNode = g_list.pFirst)->priority, 5);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 9);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 38);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 64);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 83);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 416);

	TestList_Sort(&g_list, (CompareFn_t)CompareTestListItemPriority, false);
	TestList_InsertFromFront(&g_list, &g_aNodes[0], (CompareFn_t)CompareTestListItemPriority, false);
	TestList_InsertFromBehind(&g_list, &g_aNodes[7], (CompareFn_t)CompareTestListItemPriority, false);

	printf("----------------------------------------\n");
	for (pNode = g_list.pFirst; pNode != NULL; pNode = pNode->pNext) {
		pItem = TestList_GetItem(pNode);
		printf("id = %d; priority = %d;\n", pItem->id, pItem->priority);
	}

	EXPECT_EQ(TestList_GetItem(pNode = g_list.pFirst)->priority, 416);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 83);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 64);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 38);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 17);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 9);
	EXPECT_EQ(TestList_GetItem(pNode = pNode->pNext)->priority, 5);

	EXPECT_EQ(g_list.count, 9);
}
