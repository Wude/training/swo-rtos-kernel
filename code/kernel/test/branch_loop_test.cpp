#include "gtest/gtest.h"
#include <vcruntime.h>

extern "C" {
#include "util.h"
#include <inttypes.h>

typedef struct Node Node_t;

struct Node {
	const Str_t sId;
	Node_t*     apChilds[2];
};

static const Str_t g_asBitString[] = {
	"0000", "0001", "0010", "0011", "0100", "0101", "0110", "0111",
	"1000", "1001", "1010", "1011", "1100", "1101", "1110", "1111",
};
}

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

TEST(BranchLoopTest, Iterate) {
	Node_t c1 = {.sId = "C1", .apChilds = {NULL, NULL}};
	Node_t c2 = {.sId = "C2", .apChilds = {NULL, NULL}};
	Node_t c3 = {.sId = "C3", .apChilds = {NULL, NULL}};
	Node_t c4 = {.sId = "C4", .apChilds = {NULL, NULL}};
	Node_t b1 = {.sId = "B1", .apChilds = {&c1, &c2}};
	Node_t b2 = {.sId = "B2", .apChilds = {&c3, &c4}};
	Node_t a  = {.sId = "A", .apChilds = {&b1, &b2}};

	Node_t*        pNode;
	uint32_t       branchIndex;
	uint32_t       periodIndex;
	const uint32_t periodExponent = 2; //< Depth = 3 = periodExponent + 1

	printf("--------------------------------------------------\n");
	for (branchIndex = 0; branchIndex < 4; ++branchIndex) {
		printf("branchIndex = %s;\n", g_asBitString[branchIndex]);
		for (pNode = &a, periodIndex = periodExponent; pNode != NULL;
			 pNode = pNode->apChilds[(branchIndex >> periodIndex) & 1]) {
			printf(
				"\t(branchIndex>>periodIndex)&1 = %d; pNode->sId = \"%s\";\n", (branchIndex >> periodIndex) & 1,
				pNode->sId);
			--periodIndex;
		}
	}
}
