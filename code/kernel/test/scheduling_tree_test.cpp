#include "gtest/gtest.h"

#include "util.hpp"

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdint.h>
#include <stdio.h>

extern "C" {
#include "scheduling_tree.h"

#define PRINT(indentation, format, args...) \
	for (uint32_t i = 0; i < indentation; ++i) { printf("\t"); } \
	printf(format, args);

void printPeriodicTask(PeriodicTask_t* pTask, uint32_t indentation) {
	PRINT(
		indentation, "pTask = { .base.sName = \"%s\", .periodExponent = %d, .durationMs = %d }\n", pTask->base.sName,
		pTask->periodExponent, pTask->durationMs)
}

void printSlot(SchedulingTreeSlot_t* pSlot, uint32_t indentation, bool all) {
	PRINT(indentation, "pSlot = %p; ", pSlot);
	if (pSlot->pPeriodicTask != NULL) {
		printPeriodicTask(pSlot->pPeriodicTask, 0);
	} else {
		PRINT(0, "pSlot->freeDurationMs = %d\n", pSlot->freeDurationMs);
	}

	if (all && pSlot->pNext != NULL) { printSlot(pSlot->pNext, indentation, all); }
}

void printNode(SchedulingTreeNode_t* pNode, uint32_t indentation) {
	PRINT(indentation, "pNode->branchDurationMs = %d\n", pNode->branchDurationMs);

	++indentation;
	if (pNode->pSlot != NULL) { printSlot(pNode->pSlot, indentation, true); }

	if (pNode->apChilds[0] != NULL) { printNode(pNode->apChilds[0], indentation); }
	if (pNode->apChilds[1] != NULL) { printNode(pNode->apChilds[1], indentation); }
}
}

#define CASE_EXPECT_EQ(index, val1, val2, index_action) \
	case index: \
		EXPECT_EQ(val1, val2); \
		index_action; \
		break

TEST(SchedulingTreeTest, CreateEmpty) {
	PeriodicTaskList_t       periodicTasks;
	SchedulingTree_t         schedulingTree;
	SchedulingTreeIterator_t schedulingIterator;

	PeriodicTaskList_Init(&periodicTasks, 0);
	ErrorReasonStr_t sErrorReason = SchedulingTree_Init(&schedulingTree, 10, &periodicTasks);
	if (sErrorReason == NULL) {
		SchedulingTreeSlot_t* pSlot = SchedulingTreeIterator_Init(&schedulingIterator, &schedulingTree);
		EXPECT_NE(pSlot, nullptr);
		if (pSlot->pPeriodicTask != NULL) {
			printf("pSlot->pPeriodicTask != NULL\n");
		} else {
			printf("pSlot->pPeriodicTask == NULL\n");
		}
		EXPECT_EQ(pSlot->pPeriodicTask, nullptr);
		EXPECT_EQ(pSlot->freeDurationMs, 10);
	} else {
		printf("%s\n", sErrorReason);
	}
	EXPECT_EQ(sErrorReason, nullptr);
}

TEST(SchedulingTreeTest, CreateWithLittleContent) {
	PeriodicTaskList_t       periodicTasks;
	SchedulingTree_t         schedulingTree;
	SchedulingTreeIterator_t schedulingIterator;

	PeriodicTaskList_Init(&periodicTasks, 2);
	PeriodicTask_Init(&periodicTasks.aData[0], "A", 1, 4);
	PeriodicTask_Init(&periodicTasks.aData[1], "B", 0, 3);
	ErrorReasonStr_t sErrorReason = SchedulingTree_Init(&schedulingTree, 10, &periodicTasks);
	if (sErrorReason == NULL) {
		if (schedulingTree.pRoot != NULL) {
			printf("\n----------------------------------------\n");
			PRINT(1, "schedulingTree.maxPeriodExponent = %d\n", schedulingTree.maxPeriodExponent);
			printNode(schedulingTree.pRoot, 1);

			printf("\n----------------------------------------\n");
			uint32_t i = 0;
			for (SchedulingTreeSlot_t* pSlot = SchedulingTreeIterator_Init(&schedulingIterator, &schedulingTree);
				 schedulingIterator.cycleCount == 0; pSlot = SchedulingTreeIterator_Next(&schedulingIterator)) {
				printf("iterator.branchIndex = %d; ", schedulingIterator.branchIndex);
				printSlot(pSlot, 0, false);

				switch (schedulingIterator.branchIndex) {
				case 0:
					switch (i) {
						CASE_EXPECT_EQ(0, pSlot->pPeriodicTask->base.sName, "B", ++i);
						CASE_EXPECT_EQ(1, pSlot->pPeriodicTask->base.sName, "A", ++i);
						CASE_EXPECT_EQ(2, pSlot->freeDurationMs, 3, i = 0);
					}
					break;
				case 1:
					switch (i) {
						CASE_EXPECT_EQ(0, pSlot->pPeriodicTask->base.sName, "B", ++i);
						CASE_EXPECT_EQ(1, pSlot->freeDurationMs, 7, i = 0);
					}
					break;
				}
			}

			EXPECT_EQ(schedulingTree.pRoot->apChilds[0]->pSlot->pNext->freeDurationMs, 3);
			EXPECT_EQ(schedulingTree.pRoot->apChilds[1]->pSlot->freeDurationMs, 7);
		}
	} else {
		printf("%s\n", sErrorReason);
	}
	EXPECT_EQ(sErrorReason, nullptr);
}

TEST(SchedulingTreeTest, CreateWithContent) {
	PeriodicTaskList_t       periodicTasks;
	SchedulingTree_t         schedulingTree;
	SchedulingTreeIterator_t schedulingIterator;

	PeriodicTaskList_Init(&periodicTasks, 6);
	PeriodicTask_Init(&periodicTasks.aData[0], "A", 1, 4);
	PeriodicTask_Init(&periodicTasks.aData[1], "B", 0, 3);
	PeriodicTask_Init(&periodicTasks.aData[2], "C", 2, 2);
	PeriodicTask_Init(&periodicTasks.aData[3], "D", 1, 1);
	PeriodicTask_Init(&periodicTasks.aData[4], "E", 0, 1);
	PeriodicTask_Init(&periodicTasks.aData[5], "F", 2, 5);

	printf("SchedulingTreeTest.CreateWithContent\n");
	ErrorReasonStr_t sErrorReason = SchedulingTree_Init(&schedulingTree, 10, &periodicTasks);
	printf("SchedulingTreeTest.CreateWithContent - A\n");
	if (sErrorReason == NULL) {
		if (schedulingTree.pRoot != NULL) {
			printf("\n----------------------------------------\n");
			PRINT(1, "schedulingTree.maxPeriodExponent = %d\n", schedulingTree.maxPeriodExponent);
			printNode(schedulingTree.pRoot, 1);

			printf("\n----------------------------------------\n");
			uint32_t i = 0;
			for (SchedulingTreeSlot_t* pSlot = SchedulingTreeIterator_Init(&schedulingIterator, &schedulingTree);
				 schedulingIterator.cycleCount == 0; pSlot = SchedulingTreeIterator_Next(&schedulingIterator)) {
				printf("iterator.branchIndex = %d; ", schedulingIterator.branchIndex);
				printSlot(pSlot, 0, false);

				switch (schedulingIterator.branchIndex) {
				case 0:
				case 1:
					switch (i) {
						CASE_EXPECT_EQ(0, pSlot->pPeriodicTask->base.sName, "B", ++i);
						CASE_EXPECT_EQ(1, pSlot->pPeriodicTask->base.sName, "E", ++i);
						CASE_EXPECT_EQ(2, pSlot->pPeriodicTask->base.sName, "A", ++i);
						CASE_EXPECT_EQ(3, pSlot->freeDurationMs, 2, i = 0);
					}
					break;
				case 2:
					switch (i) {
						CASE_EXPECT_EQ(0, pSlot->pPeriodicTask->base.sName, "B", ++i);
						CASE_EXPECT_EQ(1, pSlot->pPeriodicTask->base.sName, "E", ++i);
						CASE_EXPECT_EQ(2, pSlot->pPeriodicTask->base.sName, "D", ++i);
						CASE_EXPECT_EQ(3, pSlot->pPeriodicTask->base.sName, "C", ++i);
						CASE_EXPECT_EQ(4, pSlot->freeDurationMs, 3, i = 0);
					}
					break;
				case 3:
					switch (i) {
						CASE_EXPECT_EQ(0, pSlot->pPeriodicTask->base.sName, "B", ++i);
						CASE_EXPECT_EQ(1, pSlot->pPeriodicTask->base.sName, "E", ++i);
						CASE_EXPECT_EQ(2, pSlot->pPeriodicTask->base.sName, "D", ++i);
						CASE_EXPECT_EQ(3, pSlot->pPeriodicTask->base.sName, "F", i = 0);
					}
					break;
				}
			}

			EXPECT_EQ(schedulingTree.pRoot->apChilds[0]->apChilds[0]->pSlot->freeDurationMs, 2);
			EXPECT_EQ(schedulingTree.pRoot->apChilds[0]->apChilds[1]->pSlot->freeDurationMs, 2);
			EXPECT_EQ(schedulingTree.pRoot->apChilds[1]->apChilds[0]->pSlot->pNext->freeDurationMs, 3);
		}
	} else {
		printf("%s\n", sErrorReason);
	}
	EXPECT_EQ(sErrorReason, nullptr);
}
