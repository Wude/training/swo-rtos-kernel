#include "gtest/gtest.h"

extern "C" {
#include "array_list.h"
}

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

DEFINE_ARRAY_LIST(StringList, StringList_t, const char*, 4)

static StringList_t g_texts;

TEST(ArrayListTest, AddRemoveCount) {
	StringList_Init(&g_texts, 0);
	StringList_AppendItem(&g_texts, "Test 1");
	StringList_AppendItem(&g_texts, "Test 2");
	StringList_AppendItem(&g_texts, "Test 3");
	StringList_AppendItem(&g_texts, "Test 4");
	StringList_AppendItem(&g_texts, "Test 5");

	printf("g_texts.count = %d\n", g_texts.count);
	printf("g_texts.aData[1] = \"%s\"\n", g_texts.aData[1]);

	EXPECT_EQ(4, g_texts.count);
	EXPECT_EQ("Test 2", g_texts.aData[1]);

	StringList_RemoveSlice(&g_texts, 0, 3);
	EXPECT_EQ(0, g_texts.count);

	g_texts.count = 4;
	StringList_RemoveSlice(&g_texts, 1, 2);
	printf("g_texts.count = %d\n", g_texts.count);
	printf("g_texts.aData[1] = \"%s\"\n", g_texts.aData[1]);
	EXPECT_EQ("Test 4", g_texts.aData[1]);

	EXPECT_EQ(2, g_texts.count);
}
