#include "gtest/gtest.h"

extern "C" {
#include "buffer.h"
}

#include <fstream>
#include <iostream>
#include <ostream>
#include <stdio.h>

DEFINE_BUFFER(CharBuffer, CharBuffer_t, char, 4)

static CharBuffer_t g_chars;

TEST(BufferTest, WriteReadCount) {
	CharBuffer_Init(&g_chars);
	CharBuffer_Write(&g_chars, 'A');
	CharBuffer_Write(&g_chars, 'B');
	CharBuffer_Write(&g_chars, 'C');
	CharBuffer_Write(&g_chars, 'D');
	CharBuffer_Write(&g_chars, 'E');

	printf("g_chars.count = %d\n", g_chars.count);
	printf("g_chars.aData[1] = \"%c\"\n", g_chars.aData[1]);

	EXPECT_EQ(4, g_chars.count);
	EXPECT_EQ('B', g_chars.aData[1]);

	CharBuffer_Read(&g_chars);
	CharBuffer_Read(&g_chars);
	CharBuffer_Read(&g_chars);
	CharBuffer_Read(&g_chars);
	CharBuffer_Read(&g_chars);

	EXPECT_EQ(0, g_chars.count);
}
