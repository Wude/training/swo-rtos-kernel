#ifndef __UTIL_HPP__
#define __UTIL_HPP__

// Macros taken from: https://stackoverflow.com/questions/53883790/check-for-errors-in-gtest-using-void-functions

// A void test-function using  ASSERT_ or EXPECT_ calls with a custom message should be encapsulated
// by this macro. Example: CHECK_FOR_FAILURES_MSG(MyCheckForEquality(counter, 42), "for counter=42")
#define CHECK_FOR_FAILURES_MSG(statement, message) \
	{ \
		SCOPED_TRACE(message); \
		ASSERT_NO_FATAL_FAILURE((statement)); \
	}

// A void test-function using  ASSERT_ or EXPECT_ calls should be encapsulated by this macro.
// Example: CHECK_FOR_FAILURES(MyCheckForEquality(lhs, rhs))
#define CHECK_FOR_FAILURES(statement) CHECK_FOR_FAILURES_MSG(statement, " <--  line of failure\n")

#endif // __UTIL_HPP__
