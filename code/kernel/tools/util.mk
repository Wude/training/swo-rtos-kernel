# A macro for getting an upper case version of a string.
UPPER_CASE	= $(shell echo '$1' | tr '[:lower:]' '[:upper:]')

# A macro for getting an lower case version of a string.
LOWER_CASE	= $(shell echo '$1' | tr '[:upper:]' '[:lower:]')

# An empty define.
null :=

# Defines for a space.
space := ${null} ${null}
${space} := ${space}

# A define for a newline.
define \n


endef
