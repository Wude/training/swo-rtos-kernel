STDPERIPH_VERSION	?= 1.8.0

BUILD				?= Debug
# BUILD				?= Release

OCD					?= OpenOCD
# OCD					?= PyOCD

include tools/util.mk

ifneq (,$(findstring flash,$(MAKECMDGOALS)))

ifeq (openocd,$(call LOWER_CASE,$(OCD)))
include tools/openocd.mk
else
include tools/pyocd.mk
endif

else
include tools/gcc-arm-none-eabi.mk
endif
