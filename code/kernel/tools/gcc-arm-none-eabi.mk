# ----------------------------------------------------------------------------
# A makefile for compiling code with the "GNU Arm Embedded Toolchain"
# for the STM32F4 line of platforms.
# ----------------------------------------------------------------------------

BUILD_DIR	= build/$(BUILD)

MAKEDIR		= mkdir -p
CLEAN		= rm -rf

VPATH = ../..

# ----------------------------------------------------------------------------

$(BUILD_DIR): all

clean:
	$(CLEAN) build

.PHONY: $(BUILD_DIR) clean

# ----------------------------------------------------------------------------
# Make sure the make process is started in the build directory.
ifeq (,$(filter $(BUILD),$(notdir $(CURDIR))))

all:
	+@$(MAKEDIR) $(BUILD_DIR)
	+@'$(MAKE)' --no-print-directory -C $(BUILD_DIR) \
		-f '$(abspath $(lastword $(MAKEFILE_LIST)))' \
		'PROJECT=$(PROJECT)' \
		'BUILD=$(BUILD)' \
		'DEVICE_FAMILY=$(DEVICE_FAMILY)' \
		'DEVICE=$(DEVICE)' \
		'STDPERIPH_VERSION=$(STDPERIPH_VERSION)' \
		$(MAKECMDGOALS)

else

# ----------------------------------------------------------------------------
# The make process has been started in the build directory.

include ../../tools/util.mk

STDPERIPH_DIR	:= ../../lib/STM32F4xx_DSP_StdPeriph_Lib_V$(STDPERIPH_VERSION)

INCLUDES		+= -I$(STDPERIPH_DIR)/Libraries/CMSIS/Include
INCLUDES		+= -I$(STDPERIPH_DIR)/Libraries/CMSIS/Device/STSTM32F4xx/Include
INCLUDES		+= -I$(STDPERIPH_DIR)/Libraries/STM32F4xx_StdPeriph_Driver/inc
INCLUDES		+= -I../../src
SOURCES			+= $(STDPERIPH_DIR)/Libraries/CMSIS/Device/STSTM32F4xx/Source/Templates/SW4STM32/startup_$(call LOWER_CASE,$(DEVICE_FAMILY)).s
SOURCES			+= $(STDPERIPH_DIR)/Libraries/CMSIS/Device/STSTM32F4xx/Source/Templates/system_stm32f4xx.c
SOURCES			+= $(wildcard $(STDPERIPH_DIR)/Libraries/STM32F4xx_StdPeriph_Driver/src/*.s)
SOURCES			+= $(wildcard $(STDPERIPH_DIR)/Libraries/STM32F4xx_StdPeriph_Driver/src/*.c)
SOURCES			+= $(wildcard ../../src/*.c)
OBJECTS			:= $(patsubst ../../%.s,%.o,$(patsubst ../../%.c,%.o,$(SOURCES)))
LINKER_SCRIPT	:= $(wildcard $(STDPERIPH_DIR)/Project/STM32F4xx_StdPeriph_Templates/SW4STM32/$(DEVICE_FAMILY)/*.ld)

# ----------------------------------------------------------------------------

AS		= arm-none-eabi-gcc
CC		= arm-none-eabi-gcc
CPP		= arm-none-eabi-g++
LD		= arm-none-eabi-gcc
ELF2BIN	= arm-none-eabi-objcopy
PREPROC	= arm-none-eabi-cpp
OBJDUMP	= arm-none-eabi-objdump

# ----------------------------------------------------------------------------

TARGET_FLAGS	+= -mcpu=cortex-m4
TARGET_FLAGS	+= -mthumb
TARGET_FLAGS	+= -mfpu=fpv4-sp-d16
# TARGET_FLAGS	+= -mfloat-abi=soft
# TARGET_FLAGS	+= -mfloat-abi=softfp
TARGET_FLAGS	+= -mfloat-abi=hard
TARGET_FLAGS	+= -D__VFP_FP__
TARGET_FLAGS	+= -D__FPU_PRESENT
TARGET_FLAGS	+= -D__FPU_USED

ifeq ($(BUILD), Debug)
COMPILE_FLAGS	+= -g3 -O1 -DDEBUG
else
COMPILE_FLAGS	+= -O2 -DNDEBUG
endif

COMPILE_FLAGS	+= -c
COMPILE_FLAGS	+= -Wall
COMPILE_FLAGS	+= -Wno-format
COMPILE_FLAGS	+= -Wextra
COMPILE_FLAGS	+= -Wno-deprecated
COMPILE_FLAGS	+= -Wno-unused-parameter
COMPILE_FLAGS	+= -Wno-ignored-qualifiers
COMPILE_FLAGS	+= -Wno-missing-field-initializers
COMPILE_FLAGS	+= -fmessage-length=0
COMPILE_FLAGS	+= -fno-exceptions
COMPILE_FLAGS	+= -fno-builtin
COMPILE_FLAGS	+= -ffunction-sections
COMPILE_FLAGS	+= -fdata-sections
COMPILE_FLAGS	+= -funsigned-char
COMPILE_FLAGS	+= -MMD
COMPILE_FLAGS	+= -fno-delete-null-pointer-checks
COMPILE_FLAGS	+= -fomit-frame-pointer
COMPILE_FLAGS	+= -Wno-type-limits
COMPILE_FLAGS	+= -DUSE_STDPERIPH_DRIVER
COMPILE_FLAGS	+= -D$(DEVICE_FAMILY)

ASM_FLAGS		+= -x assembler-with-cpp

C_FLAGS			+= -std=gnu99

CXX_FLAGS		+= -std=gnu++98 -fno-rtti -Wvla

PREPROC_FLAGS	+= -E -P

LD_FLAGS		+= -Wl,--gc-sections
LD_FLAGS		+= -Wl,-n

ifeq ($(BUILD), Debug)
LD_FLAGS		+= --debug
# LD_FLAGS		+= --no_remove
# LD_FLAGS		+= --bestdebug
endif

LD_SYS_LIBS		+= -Wl,--start-group
LD_SYS_LIBS		+= -lstdc++
LD_SYS_LIBS		+= -lsupc++
LD_SYS_LIBS		+= -lm
LD_SYS_LIBS		+= -lc
LD_SYS_LIBS		+= -lgcc
LD_SYS_LIBS		+= -lnosys
LD_SYS_LIBS		+= -Wl,--end-group

# ----------------------------------------------------------------------------

COMPILE_FLAGS	+= $(TARGET_FLAGS)
LD_FLAGS		+= $(TARGET_FLAGS)
ASM_FLAGS		+= $(COMPILE_FLAGS)
C_FLAGS			+= $(COMPILE_FLAGS)
CPP_FLAGS		+= $(COMPILE_FLAGS)
PREPROC_FLAGS	+= $(LD_FLAGS)

# ----------------------------------------------------------------------------

.s.o:
	+@$(MAKEDIR) $(dir $@)
	+@echo "-- assemble: $(notdir $<)"
	@$(AS) -c $(ASM_FLAGS) -o $@ $<

.S.o:
	+@$(MAKEDIR) $(dir $@)
	+@echo "-- assemble: $(notdir $<)"
	@$(AS) -c $(ASM_FLAGS) -o $@ $<

.c.o:
	+@#echo "$(CURDIR)"
	+@$(MAKEDIR) $(dir $@)
	+@echo "-- compile: $(notdir $<)"
	$(CC) $(C_FLAGS) $(INCLUDES) -o $@ $<
	@#$(CC) $(C_FLAGS) $(INCLUDES) -o $@ $(patsubst ../%.c,%.c,$<)

.cpp.o:
	+@$(MAKEDIR) $(dir $@)
	+@echo "-- compile: $(notdir $<)"
	@$(CPP) $(CXX_FLAGS) $(INCLUDES) -o $@ $<

$(PROJECT).link_script.ld: $(LINKER_SCRIPT)
	@$(PREPROC) $(PREPROC_FLAGS) $< -o $@

$(PROJECT).elf: $(OBJECTS) $(SYS_OBJECTS) $(PROJECT).link_script.ld
	+@echo "$(filter %.o, $^)" > .link_options.txt
	+@echo "-- link: $(notdir $@)"
	$(LD) $(LD_FLAGS) -T $(filter-out %.o, $^) $(LIBRARIES) --output $@ @.link_options.txt $(LIBRARIES) $(LD_SYS_LIBS)

$(PROJECT).bin: $(PROJECT).elf
	$(ELF2BIN) -O binary $< $@
	+@echo "-- bin file ready to flash"

$(PROJECT).hex: $(PROJECT).elf
	$(ELF2BIN) -O ihex $< $@

$(PROJECT).elf.s: $(PROJECT).elf
	$(OBJDUMP) -S $(PROJECT).elf > $(PROJECT).elf.s

# ----------------------------------------------------------------------------

../../compile_flags.txt:
	@$(file > ../../compile_flags.txt,$(subst ${ },${\n},${C_FLAGS})${\n}-Wno-pointer-to-int-cast${\n}-Wno-unused-function${\n}-D__STATIC_INLINE=${\n}-D__GNUC__${\n}$(subst ${ },${\n},$(INCLUDES:-I../../%=-I./%)))

all: $(PROJECT).bin $(PROJECT).hex $(PROJECT).elf.s ../../compile_flags.txt

-include $(wildcard $(BUILD_DIR)/*.d)

endif
