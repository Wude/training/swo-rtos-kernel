flash:
	openocd -f board/st_nucleo_f4.cfg -c """program ./build/$(BUILD)/$(PROJECT).elf verify reset exit"""

.PHONY: flash
