\documentclass[usenames, dvipsnames, aspectratio=169]{beamer}
% \documentclass[usenames, dvipsnames]{beamer}
\usepackage[ngerman]{babel}
\usepackage{datetime}
\usepackage{graphicx}

\usepackage{tikz}
\usetikzlibrary{arrows, positioning, automata, trees}
\tikzset{
	% ->, % makes the edges directed
	% >=stealth', % makes the arrow heads bold
	% node distance=2.5cm, % specifies the minimum distance between two nodes. Change if necessary.
	every state/.style={thick, fill=yellow!20}, % sets the properties for each ’state’ node
	initial text=$ $, % sets the text that appears on the start arrow
	}

\usepackage{subcaption}
\usepackage{multicol}
\usepackage[babel,german=quotes]{csquotes}

% \usepackage[table, dvipsnames]{xcolor}
% \usepackage{xcolor}
% \definecolor{colkey}{HTML}{800000}
% \definecolor{colstr}{HTML}{406080}
% \definecolor{colcom}{HTML}{408020}

\usepackage{fontspec}
% \setmainfont{Garamond}
% \setsansfont{Open Sans}
% \setmonofont{Courier New}
% \newfontfamily\firacode{Courier New}

\newcommand{\codeop}[1]{\textcolor{red}{#1}}
\newcommand{\codeopa}[1]{\textcolor{red!60!black}{#1}}
\newcommand{\codenum}[1]{\textcolor{Plum}{#1}}

% \usetheme{FHEAI}
\usetheme{FHEAIHD}

%---------------------------------------
\usepackage{xcolor,listings}                %bindet das Paket Listings ein
\definecolor{comment}{rgb}{.15,.4,.15}     % hellgruen
\definecolor{keywd1}{rgb}{.15,.15,.6}      % dunkelblau
\definecolor{keywd2}{rgb}{.35,.5,.55}      % hellblau
\definecolor{string}{rgb}{.5,.15,.15}      % dunkelrot
\definecolor{gray}{rgb}{0.4,0.4,0.4}
\definecolor{darkblue}{rgb}{0.0,0.0,0.6}
\definecolor{cyan}{rgb}{0.0,0.6,0.6}

\lstdefinestyle{cpp}{%
	language=C++,								%hier Sprache einstellen
	basicstyle={\ttfamily} ,					%Schriftgröße
	keywordstyle=\color{blue!80!black!100},		%Farbe der keywords
	morendkeywords={NULL, uint32_t, uint64_t},%
	ndkeywordstyle=\color{darkgray}\bfseries,%
	identifierstyle=,							%Bezeichnerstyle, hier leer
	commentstyle=\color{green!50!black!100},	%Farbe der Kommentare
	stringstyle=\color{red!80!black!100},		%Aussehen der Strings
	breaklines=true,							%Automatische Zeilenumbrüche
	numbers=left,								%Zeilennummerierung links
	numberstyle=\small,							%Größe der Zeilennummerierung
	frame=single,								%einfacher Rahmen
	backgroundcolor=\color{blue!3},				%Hintergrundfarbe
	caption={Code}, 							%Standardüberschrift
	captionpos=t,								%Überschift oben (top)
	tabsize=2,
	% literate={\ \ }{{\ }}1,
}

%---------------------------------------
\newif\ifpresentation
% \presentationtrue % Sliding?

%---------------------------------------
\begin{document}

\title{RTOS-Kernel für STM32F4}
\subtitle{Projekt für das Fach \enquote{Echtzeitbetriebssysteme}}
% \author{Stefan Woyde}

\author[author1]{Stefan Woyde\\[10mm]}

\newdate{defensedate}{30}{09}{2023}
\date{\displaydate{defensedate}}
% \date{\today}

\frame{\titlepage} % generiert Titelseite aus den Metadaten

%---------------------------------------
\begin{frame}{Inhaltsverzeichnis}
	\begin{multicols}{2}
		\tableofcontents
	\end{multicols}
\end{frame}

%---------------------------------------
\section{Einstieg}
\begin{frame}{Einstieg}
	\begin{itemize}
		\ifpresentation\onslide<1->\fi
		\item Kurze und deterministische Reaktionszeiten von zentraler Bedeutung
		\ifpresentation\onslide<2->\fi
		\item RTOS \rightarrow typischerweise für Mikrokontroller
		\ifpresentation\onslide<3->\fi
		\item Periodische Tasks \rightarrow hohe Planungssicherheit
		\ifpresentation\onslide<4->\fi
		\item Sporadische \& aperiodische Tasks \rightarrow hohe Flexibilität
		\ifpresentation\onslide<5->\fi
		\item Warum nicht alles zusammen?
		\ifpresentation\onslide<6->\fi
		\item Fehlt nur noch eine automatische Planung periodischer Tasks
	\end{itemize}
\end{frame}

%---------------------------------------
\section{Task-Planung}
\begin{frame}{Task-Planung}
	\begin{itemize}
		\ifpresentation\onslide<1->\fi
		\item Benötigte periodische Tasks müssen vorab feststehen
		\ifpresentation\onslide<2->\fi
		\item Periodendauer nur als Vielfaches einer Zweierpotenz \rightarrow $T = T_{min} \times 2^{p}$
		\ifpresentation\onslide<3->\fi
		\item Geplante Dauer im Verhältnis zu den Perioden muss genug Freiraum lassen
		\ifpresentation\onslide<4->\fi
		\item Vollständiger Binärbaum gespeichert als binärer Heap \rightarrow Scheduling-Baum
		\ifpresentation\onslide<5->\fi
		\item Inhalt der Baumknoten \rightarrow Slots
			\begin{itemize}
				\item für periodische Tasks
				\ifpresentation\onslide<6->\fi
				\item oder Freiraum für sporadische oder aperiodische Tasks
			\end{itemize}
	\end{itemize}
\end{frame}

%---------------------------------------
\section{Scheduling-Baum}
\begin{frame}{Scheduling-Baum}
	\begin{itemize}
		\item Beispiel übernommen aus der Vorlesung, Abschnitt Rialto-Scheduler
		\item Periodendauer mit steigender Tiefe: 10 ms, 20 ms, 40 ms
		\item Iteration über Verzweigungsindex und Tiefe
	\end{itemize}

	\includegraphics[width=0.8\textwidth]{../images/scheduling-tree.pdf}
\end{frame}

%---------------------------------------
\section{Umsetzung}
\begin{frame}{Umsetzung}
	\begin{itemize}
		\ifpresentation\onslide<1->\fi
		\item Entwicklungsboard \enquote{NUCLEO-F446RE}, MCU \enquote{ARM® Cortex®-M4F}
		\ifpresentation\onslide<2->\fi
		\item Programmiersprache \enquote{C} mit \enquote{Inline-Assembler} (ARM-Thumb)
		\ifpresentation\onslide<3->\fi
		\item GoogleTest \rightarrow Unit-Tests für Plattform-unabhängigen Code
		\ifpresentation\onslide<4->\fi
		\item OpenOCD \rightarrow MCU-Debug
		\ifpresentation\onslide<5->\fi
		\item Scheduling-Baum: Erstellung, Iterator fürs Durchlaufen, Unit-Tests
		\ifpresentation\onslide<6->\fi
		\item Präemptives Scheduling
		\ifpresentation\onslide<7->\fi
		\item Freie Zeit ohne Verwendung \rightarrow System-Idle-Task
		\ifpresentation\onslide<8->\fi
		\item IRQ-Handler
			\begin{itemize}
				\ifpresentation\onslide<9->\fi
				\item \texttt{SysTick\_Handler} \rightarrow Zeitzählung, Anstoß zum Task-Wechsel
				\ifpresentation\onslide<10->\fi
				\item \texttt{SVC\_Handler} (System-Zugriff) \rightarrow \\
				\texttt{KernelStart}, \texttt{Yield}, \texttt{StartSporadicTask}, \texttt{StartAperiodicTask}
				\ifpresentation\onslide<11->\fi
				\item \texttt{PendSV\_Handler} \rightarrow Task-Wechsel
			\end{itemize}
	\end{itemize}
\end{frame}

%---------------------------------------
\section{Testbeispiel}

%---------------------------------------
\subsection{Start}
\begin{frame}[fragile]
	\frametitle{Testbeispiel - Start}
	\begin{itemize}
		\ifpresentation\onslide<1->\fi
		\item Start mit dem Beispiel aus der Beschreibung des Scheduling-Baums
		\ifpresentation\onslide<2->\fi
		\item Jeder Task erstattet einfach nur über UART/ITM Meldung
			\begin{itemize}
				\item Task-Name
				\item Zyklus-Index
				\item Branch-Index
				\item Systemzeit in Millisekunden
			\end{itemize}

		\ifpresentation\onslide<3->\fi
		\item Zeitfaktor in Datei \texttt{main.c} auf $1$ stellen (beim Testen genutzt für Zeitdehnung)
			\begin{lstlisting}[
				style=cpp,
				label={lis:svc_handler},
				caption={Datei \texttt{main.c}, direkt unter den Includes},
			]
			#define TIME_FACTOR 1
			\end{lstlisting}
		\ifpresentation\onslide<4->\fi
		\item Kompilieren \& Flashen
	\end{itemize}
\end{frame}

%---------------------------------------
\subsection{Logs}
\begin{frame}[fragile]
	\frametitle{Testbeispiel - Logs}
	\centering%
	Die eingefangene Ausgabe über UART (Zeile \enquote{Task-Name: Zyklus/Branch/Systime}):\\

	\begin{figure}[!ht]
		\centering%
		\begin{subfigure}[h!]{0.32\textwidth}
			\centering
			% \vspace*{-0.5cm}
			% \scalebox{0.4}{
				\includegraphics[width=0.55\textwidth]{../images/kernel-logs-1.png}
			% }
		\end{subfigure}
		% \hfill
		\begin{subfigure}[h!]{0.32\textwidth}
			\centering
			% \vspace*{-0.5cm}
			% \scalebox{0.4}{
				\includegraphics[width=0.55\textwidth]{../images/kernel-logs-2.png}
			% }
		\end{subfigure}
		% \hfill
		\begin{subfigure}[h!]{0.32\textwidth}
			\centering
			% \vspace*{-0.5cm}
			% \scalebox{0.4}{
				\includegraphics[width=0.55\textwidth]{../images/kernel-logs-3.png}
			% }
		\end{subfigure}
	\end{figure}
\end{frame}

%---------------------------------------
\begin{frame}{RTOS-Kernel für STM32F4}
	\centering
	\Large
	\emph{
		Vielen Dank für Ihre Aufmerksamkeit! \\
		Gibt es noch Fragen?
	}
\end{frame}

%---------------------------------------
\end{document}
