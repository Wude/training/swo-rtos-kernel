\chapter{Umsetzung}
\label{sec:Umsetzung}

Für die Umsetzung wurde das Buch \enquote{The Definitive Guide to ARM® Cortex®-M3 and Cortex-M4 Processors} genutzt (siehe \cite{yiu2014armcortexm3}).

% ------------------------------------------------------------------------------
\section{Scheduling-Baum}

Eine kurze Übersicht der für den Scheduling-Baum genutzten Datentypen, ist in Abbildung~\ref{fig:scheduling_tree} zu finden.

\begin{figure}[!ht]
	\centering
	\caption{Scheduler-Baum-Datentypen, Anlehnung an UML-Klassendiagramm}
	\label{fig:scheduling_tree}

	\hspace*{-1.25cm}
	\scalebox{.625}{
		\texttt{\input{../images/scheduling-tree.latex}}
	}
\end{figure}

% \clearpage

\subsection{Erstellung}

Vor Erstellung des Scheduling-Baums müssen alle periodischen Tasks bekannt gegeben werden, die später vom System ausgeführt werden sollen. Die periodischen Tasks mit der kürzesten Periodendauer werden als erste eingeplant. Der Reihe nach wird für jeden Task jeweils jene Verzweigung gewählt, für die bislang die  kürzeste Ausführungsdauer eingeplant ist.

Der erste Knoten (der auf der Höhe $1$) wird bei jeder Verzweigung durchlaufen. Die Blattknoten wiederum werden nur bei einer der Verzweigungen durchlaufen. So wird der Reihe nach jede Task- und Periodenkombination angesteuert.

Die geplante Ausführungsdauer eines Branches (einer Verzweigung) muss der kleinsten Periodendauer entsprechen. Jeder Branch wird auf die Dauer geprüft und wenn ein Branch die geplante Zeit unterschreitet, wird ein neuer Slot eingehängt. Der neue Slot hat keinen Bezug zu einem periodischen Task sondern gibt stattdessen eine freie Zeit in Millisekunden an, die später für nicht-periodische Tasks genutzt werden kann.

\subsection{Iterator}

Der Iterator soll alle Slots der Reihe nach durchschreiten. Er beginnt beim ersten Slot des Wurzelknotens. Abbildung~\ref{fig:scheduling_tree_iterator} zeigt den Datentyp des Iterators.

\begin{figure}[!ht]
	\centering
	\caption{\texttt{SchedulingTreeIterator\_t}, Anlehnung an UML-Klassendiagramm}
	\label{fig:scheduling_tree_iterator}

	\hspace*{-1.25cm}
	\scalebox{.625}{
		\texttt{\input{../images/scheduling-tree-iterator.latex}}
	}
\end{figure}

Die Auswahl des nächsten Slots wird durch die Variablen \texttt{branchIndex} und \texttt{periodIndex} gesteuert. Sie zeigen die derzeitige Verzweigung und Baumtiefe an. Das Durchschreiten der Slots beginnt immer wieder von vorn, ist also endlos. Listing~\ref{lis:schedulingtreeiterator_next} im Anhang zeigt die Funktion zur Abholung des nächsten Slots. 

% ------------------------------------------------------------------------------
\section{Task Control Block}

Der Task Control Block dient zur Steuerung eines Task-Ablaufs. Er speichert unter Anderem eine Referenz auf den genutzen Task und den ihm zugewiesenen Stack. Abbildung~\ref{fig:task_control_block} zeigt den Aufbau eines Task Control Blocks.

\begin{figure}[!ht]
	\centering
	\caption{\texttt{TaskControlBlock\_t}, Anlehnung an UML-Klassendiagramm}
	\label{fig:task_control_block}

	\hspace*{-1.25cm}
	\scalebox{.625}{
		\texttt{\input{../images/task-control-block.latex}}
	}
\end{figure}

Vor dem Start eines Tasks, muss sein Stack vorbereitet werden. Es wird ein Caller Stack Frame und ein Callee Stack Frame darauf gespeicht. Beim späteren Einwechseln durch den Scheduler werden diese Werte dann passend in die Register kopiert, wobei der Caller Stack Frame später vom System selbst verwaltet wird. Listing~\ref{lis:preparetaskstack} zeigt die Vorbereitung des Task-Starts. Als Rückkehradresse wird \texttt{Kernel\_Yield} eingetragen, sodass der Task seine Beendigung automatisch meldet und frei gewordene Zeit anderweitig genutzt werden kann.

\begin{lstlisting}[
	style=cpp,
	label={lis:preparetaskstack},
	caption={\texttt{PrepareTaskStack}},
]
#define PUSH_FRAME_FD(sp, frame_type) \
	(frame_type*)((sp) -= (sizeof(frame_type) / sizeof(uint32_t)))

/** @brief Prepare a stack.

	Prepare a "Full Descending" stack:
	- The first stack element can be found at just below the highest address.
	- The stack pointer has to be decremented before copying the stack element.

	@param [in] pTaskControlBlock The task control block referencing the stack.
*/
static void PrepareTaskStack(TaskControlBlock_t* pTaskControlBlock) {
	pTaskControlBlock->pStack = (uint32_t*)pTaskControlBlock->pStackUpper;

	CallerStackFrame_t* pCallerStackFrame =
		PUSH_FRAME_FD(pTaskControlBlock->pStack, CallerStackFrame_t);
	CalleeStackFrame_t* pCalleeStackFrame =
		PUSH_FRAME_FD(pTaskControlBlock->pStack, CalleeStackFrame_t);

	pCallerStackFrame->xPSR = XPSR_RESET_VALUE;
	pCallerStackFrame->PC   = (uint32_t)pTaskControlBlock->fAction;
	pCallerStackFrame->LR   = (uint32_t)Kernel_Yield;
	pCalleeStackFrame->LR   = RETURN_TO_THREAD_MODE_PSP;
}
\end{lstlisting}

% ------------------------------------------------------------------------------
\section{IRQ-Handler}

\subsection{\texttt{SysTick\_Handler}}

Der SysTick-Handler wird jede Millisekunde aufgerufen, er speichert die Anzahl der vergangenen Millisekunden für das gesamte System und für den derzeit aktiven Task Control Block.

Bei Erreichen eines bestimmten Zeitwerts wird ein neuer Task ausgewählt. Dabei wird der Iterator des Scheduler-Baums genutzt. Listing~\ref{lis:selectnexttask} und Listing~\ref{lis:selectnexttasknonperiodic}, jeweils im Anhang, zeigen die Auswahl eines neuen Tasks.

Für freie Zeiteinheiten wird die Einwechslung nicht-periodischer Tasks geprüft. Falls der alte Task ein nicht-periodischer war, wird er vorab wieder in die passende Warteschlange gelegt. Aperiodische Tasks werden dabei innerhalb ihrer Priorität hinten angestellt. 

Falls ein periodischer Task seine Prozessorzeit während des letzten Laufs vor Ablauf der geplanten Dauer wieder freigegeben hat, wird er hier wieder für einen frischen Start vorbereitet.

Für den Fall, es wird kein neuer Task gefunden, wird ein vom System vorbereiteter Idle-Task eingesetzt, der einfach nur eine Endlosschleife durchläuft.

Zum Abschluss des Task-Wechsels, wird der PendSV-Handler ausgelöst, der das Auswechseln übernimmt.
Listing~\ref{lis:systick_handler} zeigt den Code des SysTick-Handlers.

\begin{lstlisting}[
	style=cpp,
	label={lis:systick_handler},
	caption={\texttt{SysTick\_Handler}},
]
/** @brief The system tick handler (IRQ: SysTick). */
void SysTick_Handler() {
	// Requirements:
	// - Expected to be called every millisecond.
	// - Interrupt priority should be high (numerically low).
	++g_elapsedSystemTimeMs;
	++g_pCurrentTaskControlBlock->elapsedDurationMs;

	if (g_started && g_elapsedSystemTimeMs >= g_nextTaskSwitchTimeMs) {
		__disable_irq();
		DiscardCurrentTask();
		SelectNextTask();
		PendSV();
		__enable_irq();
	}
}
\end{lstlisting}

\subsection{\texttt{PendSV\_Handler}}

Der PendSV-Handler ist in diesem Projekt dafür zuständig, den derzeit laufenden Task gegen einen neuen Task auszutauschen.
Da der erste Member des TCB der Stack Pointer des Tasks ist, kann der Code des Handlers einfacher gestaltet werden (siehe Listing~\ref{lis:pendsv_handler} im Anhang).

Zunächst wird geprüft, ob der TCB des neuen Tasks gleich dem des Alten ist. In dem Fall wird zum Ende gesprungen.

Anderenfalls wird der laufende Task gesichert. Der wird PSP (\enquote{Process Stack Poiner}) wird abgerufen und der Callee Stack Frame wird aus den Registern heraus gespeichert. Falls der laufende Task dabei die FPU nutzte, werden auch zusätzliche FPU-Register gesichert. Der geänderte Stack Pointer wird anschließend in den TCB (Task Control Block) des laufenden Tasks gespeichert.

Anschließend wird der neue Task eingewechselt, sein Stack Pointer wird aus dem TCB geladen. Der Callee Stack Frame wird nun aus dem Stack heraus in die Register gespeichert. Falls der Task die FPU nutzte, wird zusätzlich auch in FPU-Register gespeichert. Im Anschluss wird noch der PSP auf den neuen Stack eingestellt.

\subsection{\texttt{SVC\_Handler}}

Der SVC-Handler dient als Eintrittstor für Systemanfragen. Er hat als Argument den \\ \texttt{ServiceCode}, eine Ganzzahl zwischen 0 und 255. In diesem Fall werden nur die Zahlen 0 bis 3 gewertet.

\begin{itemize}
	\item \texttt{ServiceCode\_StartKernel} (0): Start des Schedulers und Kernels
	\item \texttt{ServiceCode\_Yield} (1): Ein Task gibt seine derzeitige Zuteilung der Prozessorzeit wieder frei
	\item \texttt{ServiceCode\_StartSporadicTask} (2): Ein sporadischer Task soll gestartet werden, Task und Deadline als Argument, Ja/Nein als Rückgabe
	\item \texttt{ServiceCode\_StartAperiodicTask} (3): Ein aperiodischer Task soll gestartet werden, Task als Argument, Ja/Nein als Rückgabe
\end{itemize}

Falls ein Task seine Prozessorzeit vor Ablauf der geplanten Dauer wieder freigibt, wird die nun freigewordene Zeit für einen nicht-periodischen Task eingeteilt, soweit welche warten.

Listing~\ref{lis:svc_handler} in Anhang zeigt den Code des SVC-Handlers.

\clearpage

% ------------------------------------------------------------------------------
\section{Kernel-Vorbereitung und -Start}

In \texttt{main}, der Hauptrountine, wird zunächst der Kernel initialisiert, die internen Datenstrukturen werden vorbereitet.

Vor dem Kernel-Start müssen die später auszuführenden Tasks registriert werden. Im Fall der periodischen Tasks ist das die einzige Möglichkeit zu Registrierung. Bei Kernel-Start wird die Erstellung des Scheduler-Baums angestoßen und ein zugehöriger Iterator vorbereitet. Falls kein Problem auftrat, wird dann der erste Task ausgewählt. Der Stack Frame dieses ersten Tasks wird in die Register geladen und anschließend wird auf die Nutzung des PSP (\enquote{Process Stack Pointer}) umgestellt. An dieser Stelle bricht somit der Kontrollfluss der Kernel-Start-Funktion ab und der Scheduling-Ablauf übernimmt die Kontrolle (siehe Listing~\ref{lis:kernel_start_part}).

\begin{lstlisting}[
	style=cpp,
	label={lis:kernel_start_part},
	caption={Auszug aus \texttt{Kernel\_Start}},
]
	g_started = true;
	SVC(ServiceCode_StartKernel);

	// Switch to the "Process Stack Pointer" of the current task.
	// The double-cast is in place to avoid a linting error on a 64-bit dev host.
	__set_PSP((uint32_t)(intptr_t)g_pCurrentTaskControlBlock->pStack);
	__set_CONTROL(CONTROL_THREAD_MODE_PSP);
	__DSB();
	__ISB();
\end{lstlisting}
