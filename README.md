# RTOS-Kernel

The project was developed with `VSCodium` (libre binaries of `Visual Studio Code`).

Extension recommendations are included.

The project includes VSCode-Tasks for different purposes (build, test, etc.), click `Terminal->Run Task...` in the menu to see.

## Git

Checkout:
```sh
git clone --recurse-submodules https://gitlab.com/Wude/training/swo-rtos-kernel.git
```

Checkout of submodules if `--recurse-submodules` wasn't present when cloning:
```sh
git submodule update --init --recursive
```

## Development

* `Clangd` (included in `LLVM`): IDE support, build Tests
* `CMake` at least version 3.10: build Tests
* `GNU Make`: build Debug/Release
* `GNU Arm Embedded Toolchain`: build Debug/Release
* `OpenOCD`: MCU debug
